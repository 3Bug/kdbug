var server = require('./server');
var protocol = require('./protocol');
var leveldata = require('./data/leveldata');
var levelData = leveldata.levelData;
var _start = 0;
var _end = 0;
var _times = 1;
var id;
var connection;
var socket;
var clientInfo;

function countExp() {
    connection = server.connection;
    id = protocol.playerId;
    if (connection && id) {
        var exp = (_end - _start) / 1000 * _times;
        var update = 'update player set hangexp=hangexp+' + exp + ' where playerid="' + id + '"';
        connection.query(update, function (err, res) {
            if (err)
                console.log('update err in experience/countExp:' + err);
        });
    }
}

exports.countExp = countExp;

function changeExp(ws, ci) {
    socket = ws;
    clientInfo = ci;
    connection = server.connection;
    id = protocol.playerId;
    if (connection && id) {
        var update = 'update player set playerexperience=playerexperience+hangexp,hangexp=0 where playerid="' + id + '"';
        connection.query(update, function (err, res) {
            if (err)
                console.log('update err in experience/countExp:' + err);
            else
                changeLv();
        });
    }
}

exports.changeExp = changeExp;

function changeLv() {
    var select = 'select * from player where playerid="' + id + '"';
    connection.query(select, function (err, rows) {
        if (err)
            console.log('select err in experience/changeLv:' + err);
        else
            finalStep(rows[0]);
    });
}

function finalStep(playerObj) {
    for (var data in levelData) {
        if (playerObj.playerexperience < levelData[data].experience) {
            if (playerObj.playerlv == levelData[data - 1].level) {
                return;
            }
            else {
                levelUp(data - 1, playerObj);
                return;
            }
        }
    }
}

function levelUp(index, playerObj) {
    var point = 0;
    var varLev = levelData[index].level - playerObj.playerlv;
    var gift = [];
    if (varLev > 1) {
        for (var i = 0; i < varLev; i++) {
            point = point + levelData[index - i].point;
            if (levelData[index - i].reward != 0)
                gift = gift.concat(levelData[index - i].reward);
        }
    }
    else {
        point = levelData[index].point;
        if (levelData[index].reward != 0)
            gift = levelData[index].reward;
    }
    var updatePlayer = 'update player set playeriv=' + levelData[index].level + ',playerpoint=playerpoint+' + point + ' where playerid="' + id + '"';
    connection.query(updatePlayer, function (err, res) {
        if (err) {
            console.log('update err in experience/levelUp:' + err);
            return;
        }
        else {
            if (gift == []) {
            }
            else {
                var backpack = protocol.getBackpack(connection, id);
                if (backpack == null)
                    return;
                else if (backpack == 0)
                    noItem(gift);
                else
                    hasItem(gift, backpack);
            }
        }
    });
}

function hasItem(gift, backpack) {
    var backpackDic = [];
    for (var data in backpack) {
        backpackDic[backpack[data].id] = backpack[data];
    }
    for (var data2 in gift) {
        if (backpackDic[gift[data2].id] == null) {
            backpack.push(gift[data2]);
        }
        else {
            var exist = backpack.indexOf(backpackDic[gift[data2].id]);
            backpack[exist].num = backpack[exist].num + gift[data2].num;
        }
    }
    var json = JSON.stringify(backpack);
    var str = json.replace(/"/g, "|");
    protocol.changeBackpack(connection, socket, id, backpack, str, clientInfo.protocol);
}

function noItem(gift) {
    var json = JSON.stringify(gift);
    var str = json.replace(/"/g, "|");
    protocol.changeBackpack(connection, socket, id, gift, str, clientInfo.protocol);
}

function start(value) {
    _start = value;
}

exports.start = start;

function end(value) {
    _end = value;
}

exports.end = end;

function times(value) {
    _times = value;
}

exports.times = times;