var skilldata = require('./data/skilldata');
var skillData = skilldata.skillData;
var itemdata = require('./data/itemdata');
var itemData = itemdata.itemData;
function start(players) {
    var playerOne;
    var playerTwo;
    playerOne = {};
    playerOne.pattack = players[0].playerlv + players[0].playerpower + (players[0].playerlv + players[0].playergay) * 0.9;
    playerOne.pdefence = (players[0].playerlv + players[0].playerpower) * 0.9 + players[0].playerlv + players[0].playergay;
    playerOne.sattack = players[0].playerlv + players[0].playerintelligence + (players[0].playerlv + players[0].playernc) * 0.9;
    playerOne.sdefence = (players[0].playerlv + players[0].playerintelligence) * 0.9 + players[0].playerlv + players[0].playernc;
    playerOne.bomb = 1.5;
    playerOne.speed = 2;
    playerOne.evade = 1;
    playerOne.id = players[0].playerid;
    playerOne.lv = players[0].playerlv;
    playerOne.hp = 100 + players[0].playerhealth * 5 + (players[0].playerlv - 1) * 5;
    if (players[0].skills)
        playerOne.skill = players[0].skills.sort(function (x, y) {
            return x.position > y.position ? 1 : -1;
        });
    else
        playerOne.skill = null;
    playerTwo = {};
    playerTwo.pattack = players[1].playerlv + players[1].playerpower + (players[1].playerlv + players[1].playergay) * 0.9;
    playerTwo.pdefence = (players[1].playerlv + players[1].playerpower) * 0.9 + players[1].playerlv + players[1].playergay;
    playerTwo.sattack = players[1].playerlv + players[1].playerintelligence + (players[1].playerlv + players[1].playernc) * 0.9;
    playerTwo.sdefence = (players[1].playerlv + players[1].playerintelligence) * 0.9 + players[1].playerlv + players[1].playernc;
    playerTwo.bomb = 1.5;
    playerTwo.speed = 2;
    playerTwo.evade = 1;
    playerTwo.id = players[1].playerid;
    playerTwo.lv = players[1].playerlv;
    playerTwo.hp = 100 + players[1].playerhealth * 5 + (players[1].playerlv - 1) * 5;
    if (players[1].skills)
        playerTwo.skill = players[1].skills.sort(function (x, y) {
            return x.position > y.position ? 1 : -1;
        });
    else
        playerTwo.skill = null;
    var round = 0;
    var playerOneChangeValue;
    var playerOneSkill;
    var playerTwoChangeValue;
    var playerTwoSkill;
    var returnData = [];
    while (playerOne.hp > 0 && playerTwo.hp > 0) {
        round++;
        playerOneChangeValue = getChangeValue(playerOne.speed, playerOne.bomb);
        playerOneSkill = useSkill(playerOne.skill);
        playerOne.damage = getDamage(playerOne, playerTwo, playerOneSkill, playerOneChangeValue);
        playerTwoChangeValue = getChangeValue(playerTwo.speed, playerTwo.bomb);
        playerTwoSkill = useSkill(playerTwo.skill);
        playerTwo.damage = getDamage(playerTwo, playerOne, playerTwoSkill, playerTwoChangeValue);
        playerOne.hp = playerOne.hp - playerTwo.damage;
        playerTwo.hp = playerTwo.hp - playerOne.damage;
        var playerOneData = {
            playerid: playerOne.id,
            skillid: playerOneSkill.id,
            bomb: playerOneChangeValue,
            damage: playerOne.damage,
            health: playerOne.hp
        };
        var playerTwoData = {
            playerid: playerTwo.id,
            skillid: playerTwoSkill.id,
            bomb: playerTwoChangeValue,
            damage: playerTwo.damage,
            health: playerTwo.hp
        };
        returnData.push({roundNum: round, roundData: [playerOneData, playerTwoData]});
    }
    return returnData;
}
function useSkill(skills) {
    if (skills == null)
        return skillData[0];
    for (var data in skills) {
        for (var data2 in itemData) {
            if (itemData[data2].id == skills[data].id) {
                if (Math.random() * 99 < skillData[itemData[data2].typeValue].percent) {
                    return skillData[itemData[data2].typeValue];
                }
            }
        }
    }
    return skillData[0];
}
function getChangeValue(speed, bomb) {
    if (Math.random() * 99 < speed)
        return bomb;
    else
        return 1;
}
function getDamage(me, he, skill, changeValue) {
    if (Math.random() * 99 < he.evade || skill.id == 0)
        return 0;
    else {
        if (skill.type == 1)
            return ((me.lv * 0.4 + 2) * skill.force * me.pattack / he.pdefence / 50 + 2) * changeValue * 217 / 255;
        else if (skill.type == 2)
            return ((me.lv * 0.4 + 2) * skill.force * me.sattack / he.sdefence / 50 + 2) * changeValue * 217 / 255;
        else if (skill.type == 3) {
        }
    }
}
exports.start = start;
