var itemdata = require('./data/itemdata');
var protocol = require('./protocol');
var server = require('./server');
var itemData = itemdata.itemData;
var connection;
var socket;
var playerId;
var clientInfo;

function addItem(id, num) {
    playerId = protocol.playerId;
    connection = protocol.connection;
    socket = protocol.socket;
    clientInfo = protocol.clientInfo;
    for (var data in itemData) {
        if (itemData[data].id == id) {
            nextStep(id, itemData[data].type, num);
            return;
        }
    }
}

exports.addItem = addItem;

function nextStep(id, type, num) {
    var backpackArr = protocol.getBackpack(connection, playerId);
    if (backpackArr == null)
        return;
    else if (backpackArr == []) {
        backpackArr.push({id: id, type: type, lev: 1, num: num});
        saveNewItemsArr(backpackArr);
    }
    else {
        for (var data in backpackArr) {
            if (id == backpackArr[data].id) {
                backpackArr[data].num = backpackArr[data].num + num;
                saveNewItemsArr(backpackArr);
                return;
            }
        }
        backpackArr.push({id: id, type: type, lev: 1, num: num});
        saveNewItemsArr(backpackArr);
    }
}

function saveNewItemsArr(itemsObj) {
    var itemsJson = JSON.stringify(itemsObj);
    var str = itemsJson.replace(/"/g, "|");
    var update = 'update backpack set items="' + str + '" where id="' + playerId + '"';
    connection.query(update, function (err, res) {
        if (err) {
            console.log(err);
            var protocolInfo = {protocol: clientInfo.protocol, data: {result: 0, msg: '添加失败'}};
            sendData(socket, protocolInfo);
            return;
        }
        var protocolInfo = {protocol: clientInfo.protocol, data: {result: 1, msg: '添加成功'}};
        sendData(socket, protocolInfo);
    });
}

function sendData(socket, data) {
    var json = JSON.stringify(data);
    socket.send(json);
}

function dropItem(conn, soc, cli) {
    playerId = protocol.playerId;
    var backpackArr = protocol.getBackpack(conn, playerId);
    if (backpackArr == null) {
        var protocolInfo = {protocol: cli.protocol, data: {result: 0, msg: '查找背包数据失败'}};
        sendData(socket, protocolInfo);
    }
    else if (backpackArr == 0) {
        var protocolInfo = {protocol: cli.protocol, data: {result: 0, msg: '背包为空'}};
        sendData(socket, protocolInfo);
    }
    else {
        for (var data in backpackArr) {
            if (backpackArr[data].id == cli.data.id) {
                isTypeOne(conn, soc, cli, backpackArr[data]);
                return;
            }
        }
        var protocolInfo = {protocol: cli.protocol, data: {result: 0, msg: '背包没有该物品'}};
        sendData(socket, protocolInfo);
    }
}

function isTypeOne(conn, soc, cli, item) {
    if (cli.data.num > item.num) {
        var protocolInfo = {protocol: cli.protocol, data: {result: 0, msg: '数量不足'}};
        sendData(socket, protocolInfo);
    }
    else {

    }
}

exports.dropItem = dropItem;

function useItem() {
}

exports.useItem = useItem;