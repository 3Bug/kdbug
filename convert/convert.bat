@echo off

rem ---------------------------------------------------------
rem 控制脚本（windows版）（使用前需正确设置以下变量）
rem @author 
rem @author swyan
rem ---------------------------------------------------------

rem D:\php
set PHP=php.exe

rem 主目录
set DIR_MAIN=..

rem 服务端所在目录
set DIR_ERL=%DIR_MAIN%\server
rem client所在目录
set DIR_CLI=%DIR_MAIN%\kingdombug\src
rem 资源库所在目录
set DIR_RES=%DIR_MAIN%\kingdombug\resource\data
rem 数据所在目录
set DIR_DOC=%DIR_MAIN%\convert


goto fun_wait_input

:fun_wait_input
    echo 请输入物品表全名进行数据转换
    set inp=
    echo.
    set /p inp=请选择:
    if [%inp%]==[quit] goto fun_quit
    if [%inp%]==[dev] goto fun_dev
    if [%inp%]==[] goto fun_wait_input
    goto fun_convert_data

:fun_convert_data
        echo 到convert下
		cd %DIR_DOC%
        echo 开始转换数据
		%PHP% convert.php %inp%
		echo 数据转换成功 %inp%
		goto fun_wait_input
:end

:fun_quit
    echo "退出"
:end

:fun_dev
    cd %DIR_ERL%
    cd ..
    dev.bat
:end
