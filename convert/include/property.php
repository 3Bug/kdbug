<?php
/**
 * 基本属性策划，客户端，服务端对照配置
 * @authoer weihua
 */
function pro_sort($a, $b){
    return strlen($a[0]) - strlen($b[0]) < 0 ? 1 : -1;
}
$attr_srv_cli = array(
    array('speed', '10'),
    array('hp_max', '11'),
    array('mp_max', '12'),
    array('js', '13'),
    array('aspd', '14'),
    array('dmg', '15'),
    array('defence', '16'),
    array('dmg_magic', '17'),
    array('resist_magic', '18'),
    array('critrate', '19'),
    array('critdmg', '20'),
    array('tenacity', '21'),
    array('hitrate', '22'),
    array('evasion', '23'),
    array('hp', '24'),
    array('mp', '25'),
    array('anti_stun', '31'),
    array('anti_silent', '32'),
    array('anti_speed', '33'),
    array('anti_stone', '34'),
    array('anti_poison', '35'), 

    array('speed_per', '50'),
    array('hp_max_per', '51'),
    array('mp_max_per', '52'),
    array('js_per', '53'),
    array('aspd_per', '54'),
    array('dmg_per', '55'),
    array('defence_per', '56'),
    array('dmg_magic_per', '57'),
    array('resist_magic_per', '58'),
    array('critrate_per', '59'),
    array('critdmg_per', '60'),
    array('tenacity_per', '61'),
    array('hitrate_per', '62'),
    array('evasion_per', '63'),
    array('hp_per', '64'),
    array('mp_per', '65'),
    array('anti_stun_per', '71'),
    array('anti_silent_per', '72'),
    array('anti_speed_per', '73'),
    array('anti_stone_per', '74'),
    array('anti_poison_per', '75'),

    array('dmg_ratio', '81'), 
    array('injure_ratio', '82'), 
    array('dmg_rebound', '83'), 

    array('ratio_npc_exp', '90'), 
    array('ratio_sit_exp', '91'), 

);
usort($attr_srv_cli, "pro_sort");
return $attr_srv_cli;
