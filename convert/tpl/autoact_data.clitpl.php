<?php
/*
 * 自动播放动作
 * @author:swyan
 * @date:2015-9-1
 */
set_time_limit(0);
function writeClientFile($target_file, $xml_data, $mode='wb'){
    $oldMask  = umask(0);
    $fp       = @fopen(CLIENT_CONFIG_DIR."/kingdombug/src/core/xx/AutoActXX.ts", $mode);
	$content = "class AutoActXX {";

	$content .="\npublic static actArr: AutoActData[] = [";
    $i=0;
    $len=get_count($xml_data[$i],"act");
	for($j=2;$j<count($xml_data[$i]);$j++){
		if ($xml_data[$i][$j]['act'] != "") {
		$content .= "new AutoActData(";
		$content .= "\"".$xml_data[$i][$j]['act']."\"".",";
		$content .= $xml_data[$i][$j]['rate'].",";
		$content .= $xml_data[$i][$j]['ismove'].",";
		$content .= $xml_data[$i][$j]['times'];
		$content .=")";
		if($j<(count($xml_data[$i])-1))
		$content .=",";
		}
    }
	$content .="];";

	$content .="\npublic static talkArr: string[] = [";
    $i=1;
    $len=get_count($xml_data[$i],"talk");
	for($j=2;$j<count($xml_data[$i]);$j++){
		if ($xml_data[$i][$j]['talk'] != "") {
		$content .= "\"".$xml_data[$i][$j]['talk']."\"";
		if($j<(count($xml_data[$i])-1))
		$content .=",";
		}
    }
	$content .="];";


	$content .="\n}";
    fwrite($fp,$content);
    fclose($fp);
    umask($oldMask);
}
?>