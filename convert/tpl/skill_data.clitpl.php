<?php
/*
 * 技能数据
 * @author:swyan
 * @date:2015-9-18
 */
set_time_limit(0);
function writeClientFile($target_file, $xml_data, $mode='wb'){
    $oldMask  = umask(0);
    $fp       = @fopen(CLIENT_CONFIG_DIR."/kingdombug/src/core/xx/SkillXX.ts", $mode);
	$content = "class SkillXX {\n";
    $content .="public static skillList: SkillData[] = [";
    $i=0;
    $len=get_count($xml_data[$i],"id");
	for($j=2;$j<count($xml_data[$i]);$j++){
		if ($xml_data[$i][$j]['id'] != "") {
		$content .= "new SkillData(";
		$content .= $xml_data[$i][$j]['id'].",";
		$content .= "\"".$xml_data[$i][$j]['name']."\"".",";
		$content .= $xml_data[$i][$j]['type'].",";
		$content .= $xml_data[$i][$j]['act'].",";
		$content .= $xml_data[$i][$j]['act_time'].",";
		$content .= $xml_data[$i][$j]['act_move'].",";
		$content .= $xml_data[$i][$j]['act_key_time'].",";
		$content .= $xml_data[$i][$j]['inact'].",";
		$content .= $xml_data[$i][$j]['inact_time'].",";
		$content .= $xml_data[$i][$j]['inact_move'].",";
		$content .= $xml_data[$i][$j]['eff'].",";
		$content .= $xml_data[$i][$j]['eff_tar'];
		$content .=")";
		if($j<(count($xml_data[$i])-1))
		$content .=",";
		}
    }
	$content .="];\n";
    

	$content .="}";
	
    fwrite($fp,$content);
    fclose($fp);
    umask($oldMask);
}
?>