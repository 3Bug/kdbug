<?php
/*
 * 属性外观数据
 * @author:swyan
 * @date:2015-9-11
 */
set_time_limit(0);
function writeClientFile($target_file, $xml_data, $mode='wb'){
    $oldMask  = umask(0);
    $fp       = @fopen(CLIENT_CONFIG_DIR."/kingdombug/src/core/xx/AttrLooksXX.ts", $mode);
	$content = "class AttrLooksXX {\n";

    $content .="public static looksTypeArr: LooksTypeData[] = [";
    $i=1;
    $len=get_count($xml_data[$i],"looks_type");
	for($j=2;$j<count($xml_data[$i]);$j++){
		if ($xml_data[$i][$j]['looks_type'] != "") {
		$content .= "new LooksTypeData(";
		$content .= $xml_data[$i][$j]['looks_type'].",";
		$content .= "\"".$xml_data[$i][$j]['key_cli']."\"";
		$content .=")";
		if($j<(count($xml_data[$i])-1))
		$content .=",";
		}
    }
	$content .="];\n";

	$content .="public static attrLooksArr: AttrLooksData[] = [";
    $i=0;
    $len=get_count($xml_data[$i],"attr");
	for($j=2;$j<count($xml_data[$i]);$j++){
		if ($xml_data[$i][$j]['attr'] != "") {
		$content .= "new AttrLooksData(";
		$content .= "\"".$xml_data[$i][$j]['attr']."\"".",";
		$content .= $xml_data[$i][$j]['key'].",";
		$content .= $xml_data[$i][$j]['looks_type'].",";
		$content .= $xml_data[$i][$j]['looks'];
		$content .=")";
		if($j<(count($xml_data[$i])-1))
		$content .=",";
		}
    }
	$content .="];\n";

    

	$content .="}";
	
    fwrite($fp,$content);
    fclose($fp);
    umask($oldMask);
}
?>