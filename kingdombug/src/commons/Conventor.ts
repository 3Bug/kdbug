/**
 * 转换工具
 * @swyan 
 *
 */
class Conventor {
    private static _instance: Conventor;
    private static _allowInstance: Boolean;
    public constructor() {
        if(!Conventor._allowInstance)
            return;
    }

    public static getInstance(): Conventor {
        if(this._instance == null) {
            this._allowInstance = true;
            this._instance = new Conventor();
            this._allowInstance = false;
        }
        return this._instance;
    }

    /***将阿拉伯数字 转换为中文字**/
    public getCNValueOfSimpleInt(value: number): String {
        switch(value) {
            case 0:
                return "零";
            case 1:
                return "一";
            case 2:
                return "二";
            case 3:
                return "三";
            case 4:
                return "四";
            case 5:
                return "五";
            case 6:
                return "六";
            case 7:
                return "七";
            case 8:
                return "八";
            case 9:
                return "九";
            case 10:
                return "十";
            case 11:
                return "十一";
            case 12:
                return "十二";
        }

        return "";
    }
}
