/**
 * 服务器返回信息处理存储中心
 * @Swyin 
 * 2015-8-26
 */
class ProtocolInfo
{
    private static _instance: ProtocolInfo;
    private static _allowInstance: Boolean;
    private _finalData: Object;

    public constructor()
    {
        if ( !ProtocolInfo._allowInstance )
            return;
            
        //注册侦听协议事件
        Players.getInstance();
        this._finalData = new Object();
    }

    public static getInstance(): ProtocolInfo
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new ProtocolInfo();
            this._allowInstance = false;
        }
        return this._instance;
    }

    public handleProtocolInfo( info: string ): void
    {
        if ( info == 'heartbeat' )
            Socket.getInstance().sendHeartbeat();
        else
        {
            var data = JSON.parse( info );
            this._finalData[data.protocol] = data.data;
            GameBus.getInstance().dispatchEvent( new SocketEvent( SocketEvent.ReceiveProtocol, data.protocol ) );
        }
    }
    
    /**
    * 返回协议
    * @protocol:协议号
    * @data：数据object
    */
    public getProtocol( protocol: number ): any
    {
        if ( this._finalData[protocol] )
            return this._finalData[protocol];
        else
            return null;
    }
}