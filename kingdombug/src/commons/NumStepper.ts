/**
 * 数字渐进控件
 * @swyan 
 *
 */
class NumStepper extends egret.Sprite
{
    public static ChangeValue: string = "NumStepper.ChangeValue";

    public type: number = 1;
    private _bg: egret.Bitmap;
    private _minus: egret.Sprite;
    private _plus: egret.Sprite;
    private _text: egret.TextField;
    private _valX: number;
    private _min: number;
    private _max: number;
    private _cutNum: number;

    /**
     * type  类型
     * min  最小值
     * max 最大值
     * valX  元件间的间隔
     */
    public constructor( type: number = 1, min: number = 0, max: number = 100, valX: number = 3 )
    {
        super();
        this.type = type;
        this._valX = valX;
        this._min = min;
        this._max = max;
        this._cutNum = 0;
        this.init();
    }

    private setText(): void
    {
        if ( this._text.text != this._cutNum.toString() )
        {
            this._text.text = this._cutNum.toString();
            this.dispatchEvent( new egret.Event( NumStepper.ChangeValue ) );
        }
    }

    public setMax( val: number ): void
    {
        this._max = val;
        if ( this._cutNum >= this._max )
            this._cutNum = this._max;
        this.setText();
    }

    public setMin( val: number ): void
    {
        this._min = val;
        if ( this._cutNum <= this._min )
            this._cutNum = this._min;
        this.setText();
    }
	
    /**初始化*/
    private init(): void
    {
        this._bg = new egret.Bitmap( RES.getRes( StringUtil.substitute( "stepperBg1_png", this.type ) ) );
        this.addChild( this._bg );

        this._minus = Reflection.createSpriteBitmap( StringUtil.substitute( "stepperMinus{0}_png", this.type ) );
        this._minus.x = 0;
        this._minus.y = 0;
        this._minus.touchEnabled = true;
        this._minus.addEventListener( egret.TouchEvent.TOUCH_TAP, this.minusClick, this );
        this.addChild( this._minus );

        this._bg.x = this._minus.x + this._minus.width + this._valX;
        this._bg.y = this._minus.y + ( ( this._bg.height - this._minus.height ) >> 1 );

        this._plus = Reflection.createSpriteBitmap( StringUtil.substitute( "stepperPlus{0}_png", this.type ) );
        this._plus.x = this._bg.x + this._bg.width + this._valX;
        this._plus.y = 0;
        this._plus.touchEnabled = true;
        this._plus.addEventListener( egret.TouchEvent.TOUCH_TAP, this.plusClick, this );
        this.addChild( this._plus );

        this._text = new egret.TextField();
        this._text.size = 30;
        this._text.textColor = 0x000000;
        this._text.width = this._bg.width;
        this._text.textAlign = egret.HorizontalAlign.CENTER;
        this.setText();
        this._text.x = this._bg.x;
        this._text.y = this._bg.y + ( ( this._bg.height - this._text.height ) >> 1 );
        this.addChild( this._text );
    }

    private minusClick(): void
    {
        this._cutNum--;
        if ( this._cutNum <= this._min )
            this._cutNum = this._min;
        this.setText();
    }

    private plusClick(): void
    {
        this._cutNum++;
        if ( this._cutNum >= this._max )
            this._cutNum = this._max;
        this.setText();
    }
    
    /**当前值**/
    public get value(): number
    {
        return this._cutNum;
    }

    public dispose(): void
    {
        this._minus.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.minusClick, this );
        this._plus.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.plusClick, this );
        while ( this.numChildren > 0 )
        {
            this.removeChildAt( 0 );
        }
    }
}
