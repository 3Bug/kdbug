/**
 * 龙骨动画
 * @swyan 
 *
 */
class DragonBonesMc extends egret.Sprite
{
    /**播放动作完成**/
    public static playActCom: string = "DragonBonesMc.playActCom";

    private _skeletonData: any;
    private _textureData: any;
    private _texture: any;
    private _factory: dragonBones.EgretFactory;
    private _armature: dragonBones.FastArmature;
    public _boneJson: string;
    public _textrueJson: string;
    public _textirePng: string;
    public _mcName: string;
    public _valX: number;
    public _valY: number;
    private _atlas: dragonBones.EgretTextureAtlas;
    //private _boneName: string;
    //private _textureName: string;
    public curActName: string = "";
    private _times: number = 0;

    public constructor( boneJson: string, textrueJson: string, textirePng: string, mcName: string, valX?: number, valY?: number )
    {
        super();
        this._boneJson = boneJson;
        this._textrueJson = textrueJson;
        this._textirePng = textirePng;
        this._mcName = mcName;
        this._valX = valX;
        this._valY = valY;
        this._skeletonData = RES.getRes( boneJson );
        this._textureData = RES.getRes( textrueJson );
        this._texture = RES.getRes( textirePng );

        this._factory = new dragonBones.EgretFactory();
        this._factory.addSkeletonData( dragonBones.DataParser.parseDragonBonesData( this._skeletonData ), mcName );
        this._atlas = new dragonBones.EgretTextureAtlas( this._texture, this._textureData );
        this._factory.addTextureAtlas( this._atlas, mcName );
        this._armature = this._factory.buildFastArmature( mcName ); //构建FastArmature
        //开启数据缓存，30代表采样帧频，推荐设置为12~30，达到性能和动画流畅度的最佳平衡点。
        this._armature.enableAnimationCache( 30 );
        if ( Games.getInstance().animationCacheMgr == null )
            Games.getInstance().animationCacheMgr = this._armature.enableAnimationCache( 30 );
        else
            Games.getInstance().animationCacheMgr.bindCacheUserArmature( this._armature );

        this.addChild( this._armature.display );
        dragonBones.WorldClock.clock.add( this._armature );
    }

    private actCom( event: dragonBones.ArmatureEvent ): void
    {
        this._times--;
        //if ( this._times <= 0 )
        this.dispatchEvent( new egret.Event( DragonBonesMc.playActCom ) );
    }
   

    /***播放动作    act 动作名称   timers 次数   -1为循环播放**/
    public play( act: string, times: number = -1 ): void
    {

        if ( this._armature )
        {
            this.curActName = act;
            if ( times == -1 )
                this._armature.animation.gotoAndPlay( act );
            else
                this._armature.animation.gotoAndPlay( act, 0, 0, times );

            this._times = times;
            if ( this._times != -1 )
            {
                this._armature.addEventListener( dragonBones.ArmatureEvent.COMPLETE, this.actCom, this );
            }
        }

    }

    public stop(): void
    {
        if ( this._armature )
            this._armature.animation.stop();
    }
    
    /***设置外观**/
    public setLook( boneName: string, textureName: string ): void
    {
        if ( boneName == "" || textureName == "" )
            return;
        RES.getResByUrl( StringUtil.substitute( "{0}{1}/{2}.png", ResourcePath.role, boneName, textureName ), this.textTrueCom, this );
    }

    private textTrueCom( texture: egret.Texture, url: string = "" ): void
    {
        /*if ( url.indexOf( this._boneName ) == -1 || url.indexOf( this._textureName ) == -1 )
            return;*/
        var boneName: string = "";
        var textureName: string = "";
        var tempArr: any[] = [];
        var reg: RegExp = /\/role\/(\w+)\//ig;
        while ( null != ( tempArr = reg.exec( url ) ) )
            boneName = tempArr[1];

        var reg: RegExp = /\/(\w+).png/ig;
        while ( null != ( tempArr = reg.exec( url ) ) )
            textureName = tempArr[1];

        var _bone: dragonBones.FastBone = this._armature.getBone( boneName );
        if ( _bone == null )
            return;
        var bit: egret.Bitmap = <egret.Bitmap>_bone.slot.display;
        if ( bit )
        {
            bit.texture = texture;
            var obj: any = DbMcOffset.getOffset( textureName );
            if ( obj == null )
            {
                //没有偏移值则暂时不做操作
            }
            else
            {
                if ( obj.hasOwnProperty( "offsetX" ) )
                    bit.anchorOffsetX = obj.offsetX;
                if ( obj.hasOwnProperty( "offsetY" ) )
                    bit.anchorOffsetY = obj.offsetY;
            }
        }
    }

    public despose(): void
    {
        dragonBones.WorldClock.clock.remove( this._armature );
        this._armature.dispose();
        this._skeletonData = null;
        this._textureData = null;
        this._texture = null;
        this._factory = null;
        this._armature = null;
        this._boneJson = null;
        this._textrueJson = null;
        this._textirePng = null;
        this._mcName = null;
        this._valX = null;
        this._valY = null;
    }
}
