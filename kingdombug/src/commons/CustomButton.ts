/**
 * 按钮通用控件
 * @Swyin
 * 2015-08-21
 */
class CustomButton extends egret.Sprite
{
    private _normalPicName: string;
    private _clickPicName: string;
    private _pic: egret.Bitmap;
    public constructor()
    {
        super();
        this.init();
    }

    private init(): void
    {
        this.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.onBeginBtn, this );
        this.addEventListener( egret.TouchEvent.TOUCH_END, this.onEndBtn, this );
        this.addEventListener( egret.TouchEvent.TOUCH_RELEASE_OUTSIDE, this.onEndBtn, this );
        this.touchEnabled = true;
        this.initPic();
    }

    public dispose(): void
    {
        this.removeEventListener( egret.TouchEvent.TOUCH_BEGIN, this.onBeginBtn, this );
        this.removeEventListener( egret.TouchEvent.TOUCH_END, this.onEndBtn, this );
        this.removeEventListener( egret.TouchEvent.TOUCH_RELEASE_OUTSIDE, this.onEndBtn, this );
        this.touchEnabled = false;
        this.disposePic();
    }

    private initPic(): void
    {
        this._pic = new egret.Bitmap();
        this.addChild( this._pic );
    }

    private disposePic(): void
    {
        this.removeChild( this._pic );
        this._pic.texture = null;
        this._pic = null;
    }

    private onBeginBtn( event: egret.TouchEvent ): void
    {
        if ( RES.hasRes( this._clickPicName ) )
            this._pic.texture = RES.getRes( this._clickPicName );
    }

    private onEndBtn( event: egret.TouchEvent ): void
    {
        if ( RES.hasRes( this._normalPicName ) )
            this._pic.texture = RES.getRes( this._normalPicName );
    }

    /**设置按钮状态图片*/
    public setPic( normalPicName: string = "", clickPicName: string = "" ): void
    {
        this._normalPicName = normalPicName;
        this._clickPicName = clickPicName;
        if ( RES.hasRes( this._normalPicName ) )
            this._pic.texture = RES.getRes( this._normalPicName );
    }
}