/**
 * 计时器控制器    (尽量使用宏定义的时间);
 * @swyan 
 *
 */
class TimerMgr {
    /***1000毫秒*/
    public static t1000: number = 1000;
    
    /***100毫秒*/
    public static t100: number = 100;
    
    /***50毫秒*/
    public static t50: number = 50;
    
    /***25毫秒*/
    public static t25: number = 25;
    
    
    private static _instance: TimerMgr;
    private static _allowInstance: Boolean;

    private _timerDic: Object;
    private _funcDic: Object;

    public constructor() {
        if(!TimerMgr._allowInstance)
            return;
        this._timerDic = new Object();
        this._funcDic = new Object();
    }
    public static getInstance(): TimerMgr {
        if(this._instance == null) {
            this._allowInstance = true;
            this._instance = new TimerMgr();
            this._allowInstance = false;
        }
        return this._instance;
    }

    /***
     * 添加计时器<br>
     * delay  每次执行时间<br>
     * func   执行函数    执行函数 需要一个参数  保存自身父类如  private  timer(that:any):void{}<br>
     * parent 函数父类<br>
     */ 
    public add(delay: number,func: Function,parent: any): void {
        if(func == null)
            return;
        var key: string = delay.toString();
        var arr: any[];
        if(this._timerDic.hasOwnProperty(key)) {
            arr = this._funcDic[key];
            if(arr)
                arr.push({ f: func,p: parent });
        }
        else {
            this.createTimer(delay);
            arr = [];
            arr.push({ f: func,p: parent });
            this._funcDic[key] = arr;
        }
    }

    public remove(func: Function): void {
        var arr: any[];
        var key: any;
        var f: any;
        var i: number = 0;
        for(key in this._funcDic) {
            arr = <any[]>this._funcDic[key];
            if(arr) {
                for(i = 0;i < arr.length;i++) {
                    f = arr[i];
                    if(f && f.f == func) {
                        arr.splice(i,1);
                        break;
                    }
                }
            }
        }

        if(arr && arr.length == 0) {
            var timer: egret.Timer = this._timerDic[key];
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER,this.timerHandler,this);
            delete this._funcDic[key];
            delete this._timerDic[key];
        }
    }

    private createTimer(delay: number): egret.Timer {
        var key: string = delay.toString();
        if(this._timerDic.hasOwnProperty(key) == false) {
            var timer: egret.Timer = new egret.Timer(delay);
            timer.addEventListener(egret.TimerEvent.TIMER,this.timerHandler,this);
            timer.start();
            this._timerDic[key] = timer;
        }
        return this._timerDic[key];
    }

    private timerHandler(e: egret.TimerEvent): void {
        //	var curTimer:Number=getTimer();
        var key: string;
        var timer: egret.Timer = <egret.Timer>(e.target);
        if(timer == null)
            return;
        key = timer.delay.toString();
        var list: any[] = this._funcDic[key];
        var f: any;
        for(var i: number = 0;i < list.length;i++) {
            f = <any[]>list[i];
            if(f)
                f.f.call(f.p);
        }
    }
}
