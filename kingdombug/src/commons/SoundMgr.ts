/**
 * 声音管理器
 * @swyan 
 *
 */
class SoundMgr
{

    private static _instance: SoundMgr;
    private static _allowInstance: Boolean;

    /**背景音乐频道**/
    public static BgCannel: number = 1;
    
    /**特效音乐频道**/
    public static EffCannel: number = 2;
    
    /**背景声音**/
    private _bgSound: SoundPlayer;
    
    /**特效声音**/
    private _effSound: SoundPlayer;

    public constructor()
    {
        if ( !SoundMgr._allowInstance )
            return;

        this._bgSound = new SoundPlayer();
        this._effSound = new SoundPlayer();
    }

    public static getInstance(): SoundMgr
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new SoundMgr();
            this._allowInstance = false;
        }
        return this._instance;
    }
    
    /***播放声音**/
    public playSound( cannel: number, soundName: string ): void
    {
        var url: string = StringUtil.substitute( ResourcePath.assetsSound, soundName );
        switch ( cannel )
        {
            case SoundMgr.BgCannel:
                this._bgSound.loadSound( url, true );
                break;
            case SoundMgr.EffCannel:
                this._effSound.loadSound( url );
                break;
        }
    }
}
