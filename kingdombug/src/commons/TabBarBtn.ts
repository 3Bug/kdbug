/**
 * TabBar按钮
 * @swyan 
 *
 */
class TabBarBtn extends egret.Sprite {

    private _btnData: Object;

    private _type: number;

    private _idx: number;
    
    /**btnData 按钮数据   type 1文字按钮类型   idx序号<br>
    * 文字按钮数据类型:{lable:"按钮1",tsize:12,wid:40,hei:30}<br>
    **/
    public constructor(btnData: Object,type: number,idx: number) {
        super();
        this._btnData = btnData;
        this._type = type;
        this._idx = idx;

        switch(this._type) {
            case 1:
                this.initWordBtn();
                break;
        }
    }
    
    /***初始化文本按钮**/
    private initWordBtn() {
        var bg: egret.Sprite = new egret.Sprite();
        bg.name = "bg";
        bg.graphics.beginFill(0xff0000,0.5);
        bg.graphics.drawRoundRect(0,0,this._btnData["wid"],this._btnData["hei"],15);
        bg.graphics.endFill();
        this.addChild(bg);

        var text: egret.TextField = new egret.TextField();
        text.size = this._btnData["tsize"];
        text.text = this._btnData["lable"];
        text.x = (this._btnData["wid"] - text.width) >> 1;
        text.y = (this._btnData["hei"] - text.height) >> 1;
        this.addChild(text);
    }

    public getWid(): number {
        switch(this._type) {
            case 1:
                return this._btnData["wid"];
        }
        return 0;
    }
    
    /****设置是否选中*/
    public setChoose(b: boolean): void {
        if(this._type == 1) {
            var bg: egret.Sprite = <egret.Sprite>this.getChildByName("bg");
            bg.graphics.clear();
            if(b)
                bg.graphics.beginFill(0x00ff24,0.5);
            else
                bg.graphics.beginFill(0xff0000,0.5);
            bg.graphics.drawRoundRect(0,0,this._btnData["wid"],this._btnData["hei"],5);
            bg.graphics.endFill();
        }
        if(this._type == 2) {

        }
    }

    public dispose(): void {

    }

    public get idx(): number {
        return this._idx;
    }
}
