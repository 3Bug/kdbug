/**
 * 龙骨动画偏移
 * @swyan 
 *
 */
class DbMcOffset {
    public static offObj: any = null;
	public constructor() {
	}
	
	/***获取动画偏移值**/
	public static getOffset(key:string):any{
        if ( this.offObj == null )
            this.newOffObj();
            var obj:Object
            if ( this.offObj.hasOwnProperty( key ) )
                return this.offObj[key];
            else
                return null;
	}
	
	private static newOffObj():void{
        this.offObj = {};
    	 /**装饰部分 用资源名称 {offsetX?:0,offsetY?:0}**/
        //this.offObj.head_1_1 = {offsetX:50};//头饰1
	}
}
