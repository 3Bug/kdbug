/**
 * 图片容器(如果不存在缓存里面 会直接加载外部资源)
 * @swyan 
 *
 */
class ImgSprite extends egret.Sprite
{
    public static LoadCom: string = "ImgSprite.LoadCom";

    private _url: string = "";

    private _bit: egret.Bitmap;

    public constructor( url: string = "" )
    {
        super();
        this._url = url;
        this._bit = new egret.Bitmap();
        this.addChild( this._bit );

        if ( this._url != "" && this._url != null )
            this.setImg( this._url );
    }
    
    /***设置位图   (如果不存在缓存里面 会直接加载外部资源)***/
    public setImg( url: string ): void
    {
        this._url = url;
        //加载图片资源
        RES.getResByUrl( this._url, this.loadCom, this, RES.ResourceItem.TYPE_IMAGE );
    }

    private loadCom( texture: egret.Texture, url: string = "" ): void
    {
        //将加载完的资源进行显示
        if ( this._bit )
        {
            this._bit.texture = texture;
            this.dispatchEvent( new egret.Event( ImgSprite.LoadCom ) );
        }
    }
    
    /**位置纹理*/
    public setTexture( res: string ): void
    {
        if ( this._bit )
            this._bit.texture = RES.getRes( res );
    }

    public despose(): void
    {
        if ( this._bit )
            this._bit.texture = null;
    }
}
