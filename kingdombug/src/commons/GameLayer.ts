/**
 * 游戏层
 * @swyan 
 *
 */
class GameLayer {
    private static  _instance:GameLayer;
    private static  _allowInstance:Boolean;
    
    /***场景层**/
    private _sceneLayer: egret.Sprite;
    
    /***界面层**/
    private _viewLayer: egret.Sprite;
    
    /***ui层**/
    private _uiLayer: egret.Sprite;
    
    /***故事剧情层**/
    private _plotLayer: egret.Sprite;
    
    /***战斗层**/
    private _fightLayer: egret.Sprite;
    
    /***警告消息层**/
    private _alertLayer: egret.Sprite;
    
	public constructor() {
        if(!GameLayer._allowInstance)
            return;
            
        this._sceneLayer = new egret.Sprite();
        this._viewLayer = new egret.Sprite();
        this._uiLayer = new egret.Sprite();
        this._plotLayer = new egret.Sprite();
        this._alertLayer = new egret.Sprite();
        this._fightLayer = new egret.Sprite();
	}
	
	/***获取场景层**/
	public get sceneLayer():egret.Sprite{
        return this._sceneLayer;
	}
	
    /***获取界面层**/
    public get viewLayer():egret.Sprite{
        return this._viewLayer;
    }
    
    /***获取ui层**/
    public get uiLayer():egret.Sprite{
        return this._uiLayer;
    }
    
    /***获取故事剧情层**/
    public get plotLayer():egret.Sprite{
        return this._plotLayer;
    }
    
    /***获取战斗层**/
    public get fightLayer():egret.Sprite{
        return this._fightLayer;
    }
    
    /***获取警告消息层**/
    public get alertLayer():egret.Sprite{
        return this._alertLayer;
    }
	
    public static getInstance():GameLayer
    {
        if (this._instance == null)
        {
            this._allowInstance = true;
            this._instance = new GameLayer();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
