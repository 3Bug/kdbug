/**
 * 组件映射类
 * @swyan 
 *
 */
class Reflection
{
    private static _instance: Reflection;
    private static _allowInstance: Boolean;
    public constructor()
    {
        if ( !Reflection._allowInstance )
            return;
    }
    public static getInstance(): Reflection
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Reflection();
            this._allowInstance = false;
        }
        return this._instance;
    }
    
    /**
    * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
    */
    public static createBitmapByName( name: string ): egret.Bitmap
    {
        var result: egret.Bitmap = new egret.Bitmap();
        var texture: egret.Texture = RES.getRes( name );
        result.texture = texture;
        return result;
    }
    
    /**
    * 根据name关键字创建一个Sprite对象,里面包含一个对应的bitmap图像。
    */
    public static createSpriteBitmap( name: string, valx: number = 0, valy: number = 0 ): egret.Sprite
    {
        var sp: egret.Sprite = new egret.Sprite();
        var result: egret.Bitmap = new egret.Bitmap();
        var texture: egret.Texture = RES.getRes( name );
        result.x = valx;
        result.y = valy;
        result.texture = texture;
        sp.addChild( result );
        return sp;
    }
    
    /**
    * 根据str关键字创建一个Sprite对象,里面包含一个文本
    */
    public static createSpriteBtn( str: string, bgcolor: number = 0xe4e3e3, textColor: number = 0x000000, textSize: number = 30, wid: number = 100, hei: number = 50, elipse: number = 30 ): egret.Sprite
    {
        var sp: egret.Sprite = new egret.Sprite();
        sp.graphics.beginFill( 0x000000, 0.8 );
        sp.graphics.drawRoundRect( 0, 0, wid, hei, elipse );
        sp.graphics.endFill();
        sp.graphics.beginFill( bgcolor );
        sp.graphics.drawRoundRect( 2, 2, wid - 4, hei - 4, elipse - 2 );
        sp.graphics.endFill();
        var text: egret.TextField = new egret.TextField();
        text.textColor = textColor;
        text.size = textSize;
        text.text = str;
        text.name = "text";
        text.x = ( sp.width - text.width ) >> 1;
        text.y = ( sp.height - text.height ) >> 1;
        sp.addChild( text );
        return sp;
    }
    
    /**
    * 根据str关键字创建一个Sprite对象,里面包含一个文本
    */
    public static createSpriteInput(  bgcolor: number = 0xe4e3e3, textColor: number = 0x000000, textSize: number = 26, wid: number = 200, hei: number = 50, elipse: number = 30, maxChar: number = 6): egret.Sprite
    {
        var sp: egret.Sprite = new egret.Sprite();
        sp.graphics.beginFill( 0x000000, 0.8 );
        sp.graphics.drawRoundRect( 0, 0, wid, hei, elipse );
        sp.graphics.endFill();
        sp.graphics.beginFill( bgcolor );
        sp.graphics.drawRoundRect( 2, 2, wid - 4, hei - 4, elipse - 2 );
        sp.graphics.endFill();
        var text: egret.TextField = new egret.TextField();
        text.type = egret.TextFieldType.INPUT;
        text.name = "input";
        text.textAlign = egret.HorizontalAlign.CENTER;
        text.restrict = "^ ";
        text.maxChars = maxChar;
        text.textColor = textColor;
        text.size = textSize;
        text.width = wid;
        text.x = 0;
        text.y = 10;
        sp.addChild( text );
        return sp;
    }
}
