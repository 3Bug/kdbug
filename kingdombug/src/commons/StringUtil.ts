/**
 * 字符串内容替换公共类
 * @Swyin 
 * 2015-8-25
 */
class StringUtil {
	public constructor() {
	}
	
    public static trim (str:string):string
    {
        if (str == null)
        {
            return '';
        }
        
        var startIndex:number = 0;
        while (this.isWhitespace(str.charAt(startIndex)))
        {
            ++startIndex;
        }
        
        var endIndex:number = str.length - 1;
        while (this.isWhitespace(str.charAt(endIndex)))
        {
            --endIndex;
        }
        
        if (endIndex >= startIndex)
        {
            return str.slice(startIndex, endIndex + 1);
        }
        else
        {
            return "";
        }
    }
    
    public static trimArrayElements (value:string, delimiter:string):string
    {
        if (value != "" && value != null)
        {
            var items:string[] = value.split(delimiter);
            
            var len:number = items.length;
            for (var i:number = 0; i < len; i++)
            {
                items[i] = StringUtil.trim(items[i]);
            }
            
            if (len > 0)
            {
                value = items.join(delimiter);
            }
        }
        
        return value;
    }
    
    
    public static isWhitespace (character:string):boolean
    {
        switch (character)
        {
            case " " :
            case "\t" :
            case "\r" :
            case "\n" :
            case "\f" :
            return true;
            
            default :
            return false;
        }
    }
    
    public static substitute (str:string, ... rest):string
    {
        if (str == null)
        {
            return '';
        }
        
        // Replace all of the parameters in the msg string.
        var len:number = rest.length;
        var args:Array<any>;
        if (len == 1 && rest[0] instanceof Array)
        {
            args = <Array<any>>rest[0];
            len = args.length;
        }
        else
        {
            args = rest;
        }
        
        for (var i:number = 0; i < len; i++)
        {
            str = str.replace(new RegExp("\\{" + i + "\\}","g"),args[i]);
        }
        
        return str;
    }
}
