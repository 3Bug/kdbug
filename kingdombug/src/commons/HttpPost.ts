/**
 * http请求数据
 * @swyan 
 *
 */
class HttpPost extends egret.DisplayObjectContainer
{
    private static _instance: HttpPost;
    private static _allowInstance: Boolean;
    private loader: egret.URLLoader;

    public constructor()
    {
        if ( !HttpPost._allowInstance )
            return;
        super();
        var url: string = "http://api.egret-labs.org/games/api.php";
        this.loader = new egret.URLLoader();
        this.loader.dataFormat = egret.URLLoaderDataFormat.TEXT;
        this.loader.addEventListener( egret.Event.COMPLETE, this.onPostComplete, this );
        var request: egret.URLRequest = new egret.URLRequest( url );
        request.method = egret.URLRequestMethod.POST;
        var time: number = new Date().getTime();
        var sign: string = StringUtil.substitute( "action=user.getInfoappId=404serverId=1time={1}token={0}{2}"
            , Players.getInstance().rid, time, "EJqLp1RKtoRWoXPxzztu2" );
        //console.log( sign );
        var vars: egret.URLVariables = new egret.URLVariables();
        vars.decode( "action=user.getInfo" );
        vars.decode( "appId=404" );
        vars.decode( "time=" + time.toString() );
        vars.decode( "serverId=1" );
        vars.decode( "sign=" + new md5().hex_md5( sign ) );
        vars.decode( "token=" + Players.getInstance().rid );
        request.data = vars;
        this.loader.load( request );
    }

    private onPostComplete( event: egret.Event ): void
    {
        var loader: egret.URLLoader = <egret.URLLoader> event.target;
        var obj: Object = JSON.parse( loader.data );
        if ( obj["status"] == 0 )
        {
            console.log( "status" + obj["status"] );
        }
        else
            console.log( "status" + obj["status"] + "msg" + obj["data"]["msg"] );
    }

    public static getInstance(): HttpPost
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new HttpPost();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
