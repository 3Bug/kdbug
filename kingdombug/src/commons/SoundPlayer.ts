/**
 * 声音播放器
 * @swyan     
 *
 */
class SoundPlayer
{
    private _sound: egret.Sound;
    private _url: string = "";
    public isLoadComPlay: boolean;
    public loop: boolean;

    public constructor( url: string = "" )
    {
        this._url = url;
        if ( this._url != "" )
            this.loadSound( this._url );
    }

    /****加载声音**/
    public loadSound( url: string, loop: boolean = false, isLoadComPlay: boolean = true ): void
    {
        this._url = url;
        this.loop = loop;
        this.isLoadComPlay = isLoadComPlay;
        RES.getResByUrl( this._url, this.loadCom, this, RES.ResourceItem.TYPE_SOUND );
    }

    public get url(): string
    {
        return this._url;
    }
    
    /**加载完成**/
    private loadCom( sound: egret.Sound, url: string = "" ): void
    {
        this._sound = sound;
        if ( this.isLoadComPlay && this._sound )
            this.play( this.loop );
    }

    public play( loop: boolean = false ): void
    {
        this.loop = loop;
        /*if ( this._sound )
            this._sound.play( this.loop );*/
    }

    public stop(): void
    {
        /*if ( this._sound )
            this._sound.stop();*/
    }

    public pause(): void
    {
        /*if ( this._sound )
            this._sound.pause();*/
    }

    public despose(): void
    {
        /*if ( this._sound )
            this._sound.destroy();*/
    }
}
