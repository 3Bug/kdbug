/**
 * 资源路径
 * @swyan 
 *
 */
class ResourcePath {
    
    /***资源路径**/
    public static assets: string = "resource/assets/";
    
    /***资源里Png路径**/
    public static assetsPng: string = "resource/assets/{0}.png";
    
    /***资源里jpg路径**/
    public static assetsJpg: string = "resource/assets/{0}.jpg";
    
    /***声音路径**/
    public static assetsSound: string = "resource/assets/sound/{0}.mp3";
    
    /**剧情路径**/
    public static plotHead: string = "resource/assets/plot/{0}.png";
    
    /**角色资源路径**/
    public static role: string = "resource/assets/role/";
    
    /**背景图资源路径jpg**/
    public static bgJpg: string = "resource/assets/bg/{0}.jpg";
    
    /**背景图资源路径jpg**/
    public static bgPng: string = "resource/assets/bg/{0}.png";
    
    /**背景图资源路径jpg**/
    public static Icon: string = "resource/assets/icon/{0}.jpg";
    
	public constructor() {
	}
}
