/**
 * 游戏总线
 * @swyan 
 *
 */
class GameBus extends egret.EventDispatcher{
    private static  _instance:GameBus;
    private static  _allowInstance:Boolean;
	public constructor() {
        if(!GameBus._allowInstance)
            return;
        super();
	}
    public static getInstance():GameBus
    {
        if (this._instance == null)
        {
            this._allowInstance = true;
            this._instance = new GameBus();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
