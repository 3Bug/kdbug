/**
 * 资源群组
 * @swyan 
 *
 */
class ResGroup {
    
    /**角色属性界面**/
    public static RoleView: string = "rolekview";
    
    /**背包界面**/
    public static BackPackView: string = "backpackview";
    
    /**战斗界面**/
    public static FightView: string = "fightview";
    
    /**设置界面**/
    public static SettingView: string = "settingview";
    
	public constructor() {
	}
}
