/**
 * 协议号
 * @Swyin
 * 2015_8_26
 */
class Protocol
{
    /**
    * 协议号：10000
    * @请求：playerid
    * @返回：playerid
    *       playername
    *       playerlv
    *       playerexperienc
    *       playerbyte
    *       playerhealth
    *       playerpower
    *       playerintellige
    *       playernc
    *       playergay
    *       playerpoint
    *       hangexp
    */
    public static PLAYER_INFO: number = 10000;
    
    /**
    * 协议号：10001
    * @请求：playerid,playername,playerpower,playerintelligence,playernc,playergay
    * @返回：result,msg
    */
    public static CREATE_PLAYER: number = 10001;
    
    /**
    * 协议号：10002
    * @请求：playerid
    * @返回：result,msg,playerpoint(潜能点)
    */
    public static LOGIN_STATUS: number = 10002;
    
    /**
    * 协议号：10003 (请求PVP战斗)
    * @请求：playerid1,playerid2
    * @返回：playerinfo1(object)//返回10000的信息
    *       playerinfo2(object)//返回10000的信息
    *       result(int)//0:失败，1：成功
    *       msg(string)
    *       battleData(arrary)//战斗数据数组
    *           roundMum(int)//战斗第几回合
    *           roundData(arrary)//该回合双方数据素组
    *               playerid(string)//玩家id
    *               skillid(int)//技能id
    *               bomb(number)//暴击倍数
    *               damage(number)//总伤害（如果是0，则对方闪避成功）
    *               health(number)//该回合剩余血量
    */
    public static START_BATTLE: number = 10003;
    
    /**
    * 协议号：10004 (请求更新属性)
    * @请求：playerid,playerhealth,playerpower,playerintelligence,playergay,playernv
    * @返回：result(int)//0:失败，1：成功,3:无法获取玩家信息，4：有作弊嫌疑
    *       msg(string)
    */
    public static UPDATE_ATTR: number = 10004;
    
    /**
    * 协议号：10005 (请求背包列表)
    * @请求：
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    *       items(array)
    *           id(int)//物品id
    *           type(int)//物品类型
    *           lev(int)//物品等级
    *           num(int)//物品数量
    */
    public static BACKPACK: number = 10005;
    
    /**
    * 协议号：10006 (请求添加物品到背包，测试用)
    * @请求：id(int,物品id)
    *        num(num,物品数量)
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    */
    public static ADD_ITEM: number = 10006;
    
    /**
    * 协议号：10007 (请求安装技能)
    * @请求：id(int,物品id)
    *       position(int,技能位置1-10)
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    *       skills(array)
    *           id(int)//物品id
    *           position(int)//技能位置
    *           lev(int)//技能等级
    */
    public static INSTALL_SKILL: number = 10007;
    
    /**
    * 协议号：10008 (请求安装外观)
    * @请求：id(int,物品id)
    *       type(int,外观类型，客户端定义，服务端只负责保存)
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    *       appearances(array)
    *           id(int)//物品id
    *           type(int)//外观类型
    */
    public static INSTALL_APPEARANCE: number = 10008;

    /**
    * 协议号：10009 (请求外观列表)
    * @请求：
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    *       appearances(array)
    *           id(int)//物品id
    *           type(int)//外观类型
    */
    public static APPEARANCE: number = 10009;
    
    /**
    * 协议号：10010 (请求技能列表)
    * @请求：
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    *       skills(array)
    *           id(int)//物品id
    *           position(int)//技能位置
    *           lev(int)//技能等级
    */
    public static SKILL: number = 10010;
    
    /**
    * 协议号：10011(请求获取挂机经验)
    * @请求：
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    */
    public static GET_HANG_EXP: number = 10011;
    
    /**
    * 协议号：10012(请求使用或丢弃物品,装备和外观的使用不走这一条,只能使用type：1的普通物品物品)
    * @请求：type(int)//0:丢弃，1：使用
    *       id(int),物品id
    *       num(int),使用或丢弃数量
    * @返回：result(int)//0:失败，1：成功
    *       msg(string)
    */
    public static USE_OR_DROP_ITEM: number = 10012;

    public constructor()
    {
    }
}