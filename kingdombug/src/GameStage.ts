/**
 * 游戏舞台
 * @swyan 
 *
 */
class GameStage extends egret.Sprite
{
    private _scene: egret.Sprite;
    private _view: egret.Sprite;
    private _ui: egret.Sprite;
    private _plot: egret.Sprite;
    private _fight: egret.Sprite;
    private _alert: egret.Sprite;
    public constructor()
    {
        super();
        /*this.graphics.beginFill( 0xffffff );
        this.graphics.drawRect( 0, 0, Config.stageWid, Config.stageHei );
        this.graphics.endFill();*/
        this.initLayer();
        this.initView();
        this.initUI();
        this.initPlot();
        //SoundMgr.getInstance().playSound(SoundMgr.BgCannel,"bg");//测试的时候先屏蔽 因为我要听歌
    }
	
    /**初始化游戏层**/
    private initLayer(): void
    {
        this._scene = GameLayer.getInstance().sceneLayer;
        this.addChild( this._scene );
        this._view = GameLayer.getInstance().viewLayer;
        this.addChild( this._view );
        this._ui = GameLayer.getInstance().uiLayer;
        this.addChild( this._ui );
        this._plot = GameLayer.getInstance().plotLayer;
        this.addChild( this._plot );
        this._fight = GameLayer.getInstance().fightLayer;
        this.addChild( this._fight );
        this._alert = GameLayer.getInstance().alertLayer;
        this.addChild( this._alert );
    }
	
    /**初始化界面**/
    private initView(): void
    {
        var view: MainView = new MainView();
        view.open();
        this._view.addChild( view );
    }
    
    /**初始化ui**/
    private initUI(): void
    {
        var ui: UIPanel = new UIPanel();
        ui.show();
        this._ui.addChild( ui );
    }
    
    /**初始化剧情**/
    private initPlot(): void
    {
        PlotMgr.getInstance();
    }
}
