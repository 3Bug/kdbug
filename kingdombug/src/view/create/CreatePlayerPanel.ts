/**
 * 创建角色容器
 * @swyan 
 *
 */
class CreatePlayerPanel extends PanelBase
{
    private _sureBtn: egret.Sprite;
    private _point: number = 0;
    private _pointText: egret.TextField;
    private _bug: Bug;
    private _pow: NumStepper;//力量
    private _itg: NumStepper;//智商
    private _rnc: NumStepper;//脑残值
    private _gay: NumStepper;//gay值
    private _name: egret.Sprite;

    public constructor( point: number )
    {
        super();
        this._point = point;
    }

    public show(): void
    {
        this._pointText.text = this._point.toString();
        this._bug.playAct( "stay1" );
    }

    public hide(): void
    {
        this._pow.removeEventListener( NumStepper.ChangeValue, this.updatePoint, this );
        this._bug.dispose();
        while ( this.numChildren > 0 )
        {
            this.removeChildAt( 0 );
        }
    }
    public init(): void
    {
        this.graphics.beginFill( 0x000000 );
        this.graphics.drawRect( 0, 0, Config.stageWid, Config.stageHei );
        this.graphics.endFill();
        this.graphics.beginFill( 0x8a8a8a, 0.7 );
        this.graphics.drawRoundRect( 38, 298, 404, 334, 32 );
        this.graphics.endFill();
        this.graphics.beginFill( 0x929292, 1 );
        this.graphics.drawRoundRect( 40, 300, 400, 330, 30 );
        this.graphics.endFill();

        var text: egret.TextField = new egret.TextField();
        text.textColor = 0x000000;
        text.lineSpacing = 20;
        text.size = 30;
        text.x = 150;
        text.y = 320;
        text.text = "力量:\n智商:\n脑残:\nGay:\n剩余芯片:";
        this.addChild( text );

        this._pow = new NumStepper();
        this._pow.x = 220;
        this._pow.y = 312;
        this._pow.addEventListener( NumStepper.ChangeValue, this.updatePoint, this );
        this.addChild( this._pow );

        this._itg = new NumStepper();
        this._itg.x = 220;
        this._itg.y = 362;
        this._itg.addEventListener( NumStepper.ChangeValue, this.updatePoint, this );
        this.addChild( this._itg );

        this._rnc = new NumStepper();
        this._rnc.x = 220;
        this._rnc.y = 412;
        this._rnc.addEventListener( NumStepper.ChangeValue, this.updatePoint, this );
        this.addChild( this._rnc );

        this._gay = new NumStepper();
        this._gay.x = 220;
        this._gay.y = 462;
        this._gay.addEventListener( NumStepper.ChangeValue, this.updatePoint, this );
        this.addChild( this._gay );

        this._pointText = new egret.TextField();
        this._pointText.textColor = 0x000000;
        this._pointText.x = 300;
        this._pointText.y = 520;
        this.addChild( this._pointText );

        text = new egret.TextField();
        text.textColor = 0x000000;
        text.size = 20;
        text.x = 55;
        text.y = 570;
        text.text = "BUG\n名字";
        this.addChild( text );

        this._name = Reflection.createSpriteInput();
        this._name.x = 110;
        this._name.y = 565;
        this.addChild( this._name );

        this._sureBtn = Reflection.createSpriteBtn( "确定" );
        this._sureBtn.x = 320;
        this._sureBtn.y = 565;
        this._sureBtn.touchEnabled = true;
        this._sureBtn.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onClick, this );
        this.addChild( this._sureBtn );

        this._bug = new Bug();
        this._bug.x = 90;
        this._bug.y = 530;
        this.addChild( this._bug );
    }
    
    /***更新点数*/
    private updatePoint(): void
    {
        var lastPoint: number = this._point - this._pow.value - this._itg.value - this._rnc.value - this._gay.value;
        if ( lastPoint < 0 )
            lastPoint = 0;
        this._pointText.text = lastPoint.toString();
        this._pow.setMax( this._pow.value + lastPoint );
        this._itg.setMax( this._itg.value + lastPoint );
        this._rnc.setMax( this._rnc.value + lastPoint );
        this._gay.setMax( this._gay.value + lastPoint );

        this._bug.resetBugBaseLooks();
        //更新形象
        var looks: Object = Players.getInstance().getProLooksNum("playerpower",this._pow.value);
        if ( looks && looks.hasOwnProperty( "looks" ) )
            this._bug.setLook(looks["looksType"],looks["looks"]);
            
        looks = Players.getInstance().getProLooksNum("playerintellige",this._itg.value);
        if ( looks && looks.hasOwnProperty( "looks" ) )
            this._bug.setLook(looks["looksType"],looks["looks"]);
            
        looks = Players.getInstance().getProLooksNum("playernc",this._rnc.value);
        if ( looks && looks.hasOwnProperty( "looks" ) )
            this._bug.setLook(looks["looksType"],looks["looks"]);
            
        looks = Players.getInstance().getProLooksNum("playergay",this._gay.value);
        if ( looks && looks.hasOwnProperty( "looks" ) )
            this._bug.setLook(looks["looksType"],looks["looks"]);
    }

    private onClick( event: egret.TouchEvent ): void
    {
        var input: egret.TextField = <egret.TextField>this._name.getChildByName( "input" );
        if ( input == null || input.text == "" )
        {
            OperateTipMgr.getInstance().showTips("请输入名字");
            return;
        }
            
        var obj: Object = new Object;
        obj["playerid"] = Players.getInstance().rid;
        obj["playername"] = input.text;
        obj["playerpower"] = this._pow.value;
        obj["playerintelligence"] = this._itg.value;
        obj["playernc"] = this._rnc.value;
        obj["playergay"] = this._gay.value;
        Socket.getInstance().sendProtocol( Protocol.CREATE_PLAYER, obj );
    }

}
