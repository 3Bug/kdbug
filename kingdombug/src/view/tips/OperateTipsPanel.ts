/**
 * 操作提示容器
 * @swyan 
 *
 */
class OperateTipsPanel extends egret.Sprite
{
    private _bg: egret.Sprite;
    private _text: egret.TextField;

    public constructor()
    {
        super();
        this.init();
    }
    
    /***设置内容**/
    public setContent( str: string ): void
    {
        this._text.text = "";
        this._bg.graphics.clear();

        this._text.text = str;
        this._bg.graphics.beginFill( 0x000000, 0.7 );
        this._bg.graphics.drawRoundRect( 0, 0, this._text.width + 20, this._text.height + 20, 10 );
        this._bg.graphics.endFill();
        this._text.x = 10;
        this._text.y = 10;
    }

    private init(): void
    {
        this._bg = new egret.Sprite();
        this.addChild( this._bg );

        this._text = new egret.TextField();
        this._text.textColor = 0xff9999;
        this._text.size = 15;
        this._text.text = "";
        this.addChild( this._text );
    }
}
