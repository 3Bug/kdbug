/**
 * UI界面
 * @swyan 
 *
 */
class UIPanel extends PanelBase
{
    private _btnPanel: UIBtnPanel;

    public constructor()
    {
        super();
    }

    public show(): void
    {
        this._btnPanel.show();
    }

    public hide(): void
    {
        this._btnPanel.hide();
    }

    public init(): void
    {
        this._btnPanel = new UIBtnPanel();
        this._btnPanel.y = Config.stageHei - this._btnPanel.height;
        this.addChild( this._btnPanel );
    }

}
