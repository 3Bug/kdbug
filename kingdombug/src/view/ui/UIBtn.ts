/**
 * UI按钮
 * @swyan 
 *
 */
class UIBtn extends egret.Sprite
{
    public type: number;
    private _bit: egret.Bitmap;
    private _chooseBit: egret.Bitmap;

    public constructor( viewType: number )
    {
        super();
        this.type = viewType;

        this._chooseBit = Reflection.createBitmapByName( "chooseFlag_png" );
        this._chooseBit.x = 0;
        this._chooseBit.y = 0;
        this.addChild( this._chooseBit );

        this._bit = new egret.Bitmap( RES.getRes( this.getResName() ) );
        this._bit.x = this._chooseBit.x + ( ( this._chooseBit.width - this._bit.width ) >> 1 );
        this.addChild( this._bit );
    }

    public dispose(): void
    {
        this._chooseBit.texture = null;
        this._bit.texture = null;
        this._chooseBit = null;
        this._bit = null;
    }

    /**设置是否选中*/
    public setChoose( b: boolean ): void
    {
        this._chooseBit.visible = b;
    }

    private getResName(): string
    {
        switch ( this.type )
        {
            case ViewType.SceneView:
                return "sceneBtn_png";
            case ViewType.RoleView:
                return "roleBtn_png";
            case ViewType.BagView:
                return "bagBtn_png";
            case ViewType.FightView:
                return "fightBtn_png";
            case ViewType.SettingView:
                return "settingBtn_png";
        }
        return "";
    }
}
