/**
 * UI按钮容器
 * @swyan 
 *
 */
class UIBtnPanel extends PanelBase
{
    private static viewTypeArr: number[] = [ViewType.SceneView, ViewType.RoleView, ViewType.BagView, ViewType.FightView, ViewType.SettingView];
    private _leftBtn: egret.Sprite;
    private _rightBtn: egret.Sprite;

    private _btnSp: egret.Sprite;
    private _curChooseBtn: UIBtn;

    public constructor()
    {
        super();
    }

    public show(): void
    {

    }

    public hide(): void
    {
        this.removeBtn();
    }


    public init(): void
    {
        this.graphics.beginFill( 0x000000, 0.8 );
        this.graphics.drawRect( 0, 0, Config.stageWid, 1 );
        this.graphics.endFill();

        this.graphics.beginFill( 0xdbdada, 0.8 );
        this.graphics.drawRect( 0, 1, Config.stageWid, 80 );
        this.graphics.endFill();

        /*var bit: egret.Bitmap = Reflection.createBitmapByName( "arrow_png" );
        this._leftBtn = new egret.Sprite();
        this._leftBtn.addChild( bit );
        this._leftBtn.x = 5;
        this._leftBtn.y = 18;
        this._leftBtn.touchEnabled = true;
        this.addChild( this._leftBtn );

        bit = Reflection.createBitmapByName( "arrow_png" );
        bit.scaleX = -1;
        bit.x = bit.width;
        this._rightBtn = new egret.Sprite();
        this._rightBtn.addChild( bit );
        this._rightBtn.x = Config.stageWid - 5 - this._rightBtn.width;
        this._rightBtn.y = 18;
        this._rightBtn.touchEnabled = true;
        this.addChild( this._rightBtn );*/

        this._btnSp = new egret.Sprite();
        this._btnSp.x = 35;
        this._btnSp.y = 5;
        this.addChild( this._btnSp );
        this.addBtn();
    }

    private addBtn(): void
    {
        var arr: number[] = UIBtnPanel.viewTypeArr;
        var btn: UIBtn;
        for ( var i: number = 0; i < arr.length; i++ )
        {
            btn = new UIBtn( arr[i] );
            btn.x = i * 85;
            btn.touchEnabled = true;
            btn.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onBtnClick, this );
            if ( arr[i] == ViewType.SceneView )
            {
                btn.dispatchEvent( new egret.TouchEvent( egret.TouchEvent.TOUCH_TAP ) );
                btn.setChoose( true );
                this._curChooseBtn = btn;
            }
            else
                btn.setChoose( false );
            this._btnSp.addChild( btn );
        }
    }

    private removeBtn(): void
    {
        var btn: UIBtn;
        while ( this._btnSp.numChildren > 0 )
        {
            btn = <UIBtn>this._btnSp.getChildAt( 0 );
            if ( btn )
            {
                btn.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.onBtnClick, this );
                btn.dispose();
            }

            this._btnSp.removeChildAt( 0 );
        }
    }

    private onBtnClick( event: egret.TouchEvent ): void
    {
        var btn: UIBtn = <UIBtn>event.currentTarget;
        if ( btn )
        {
            if ( this._curChooseBtn )
                this._curChooseBtn.setChoose( false );
            btn.setChoose( true );
            this._curChooseBtn = btn;
            GameBus.getInstance().dispatchEvent( new ViewEvent( ViewEvent.OpenView, btn.type ) );
        }
    }

}
