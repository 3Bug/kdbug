/**
 * 主界面容器
 * @swyan 
 *
 */
class MainView extends ViewBase
{
    /**当前页签**/
    private _curViewType: number = 0;
        
    /**当前界面**/
    private _curView: ViewBase;
    private _oldView: ViewBase;
    
    /**场景界面**/
    private _sceneView: ViewBase;
    
    /**角色界面**/
    private _roleView: ViewBase;
    
    /**硬盘界面**/
    private _bagView: ViewBase;
    
    /**战斗界面**/
    private _fbView: ViewBase;
    
    /**设定界面**/
    private _settingView: ViewBase;

    private _isTweenToClose: boolean = false;
    private _isTweenToOpen: boolean = false;

    public constructor()
    {
        super();
    }

    public open(): void
    {
        GameBus.getInstance().addEventListener(ViewEvent.OpenView, this.openView, this);
        GameBus.getInstance().addEventListener(ViewEvent.CloseView, this.closeView, this);
        TimerMgr.getInstance().add(TimerMgr.t25, this.ticker, this);
    }

    private ticker(): void
    {
        dragonBones.WorldClock.clock.advanceTime(0.01);
    }

    public close(): void
    {
        TimerMgr.getInstance().remove(this.ticker);
        GameBus.getInstance().removeEventListener(ViewEvent.OpenView, this.openView, this);
        GameBus.getInstance().removeEventListener(ViewEvent.CloseView, this.closeView, this);
    }

    private openView(event: ViewEvent): void
    {
        this.setView(event.viewType);
    }

    private closeView(event: ViewEvent): void
    {
        //目前只要是关闭任何界面 都会默认打开属性界面
        this.setView(ViewType.RoleView);
    }
    
    /***设置界面***/
    public setView(viewType: number): void
    {
        if(this._curViewType == viewType)
            return;//相同界面 不重复打开
        this._curViewType = viewType;

        if(this._isTweenToClose) {
            egret.Tween.removeTweens(this._oldView);
            this.tweenToClose(this._oldView);
        }
        if(this._isTweenToOpen) {
            egret.Tween.removeTweens(this._curView);
            this.tweenToOpen(this._curView);
        }
        if(this._curView) {//缓动移除当前界面
            this._curView.close();
            this._oldView = this._curView;
            this._oldView.x = 0;
            this._oldView.alpha = 1;
            egret.Tween.get(this._oldView).to({ x: 0 - Config.stageWid, alpha: 0.5 }, 400, egret.Ease.backIn).call(this.tweenToClose, this, [this._oldView]);
            this._isTweenToClose = true;
        }
        switch(viewType) {
            case ViewType.SceneView:
                if(this._sceneView == null)
                    this._sceneView = new SceneView();
                this._curView = this._sceneView;
                break;
            case ViewType.RoleView:
                if(this._roleView == null)
                    this._roleView = new RoleView();
                this._curView = this._roleView;
                break;
            case ViewType.BagView:
                if(this._bagView == null)
                    this._bagView = new BagView();
                this._curView = this._bagView;
                break;
            case ViewType.FightView:
                if(this._fbView == null)
                    this._fbView = new FbView();
                this._curView = this._fbView;
                break;
            case ViewType.SettingView:
                if(this._settingView == null)
                    this._settingView = new SettingView();
                this._curView = this._settingView;
                break;
        }

        if(this._curView) {//缓动添加界面
            if(this.contains(this._curView) == false)
                this.addChild(this._curView);
            this._curView.x = Config.stageWid;
            this._curView.alpha = 1;
            egret.Tween.get(this._curView).to({ x: 0 }, 400, egret.Ease.backIn).call(this.tweenToOpen, this, [this._curView]);
        }
    }

    /**缓动完成 移除界面**/
    private tweenToClose(view?: ViewBase): void
    {
        this._isTweenToClose = false;
        if(view) {
            //view.close();
            if(this.contains(view))
                this.removeChild(view);
        }
    }

    /**缓动完成 打开界面**/
    private tweenToOpen(view?: ViewBase): void
    {
        this._isTweenToOpen = false;
        if(view)
            view.open();
    }
}
