/**
 * 设置界面
 * @swyan 
 *
 */
class SettingView extends ViewBase
{
    public constructor()
    {
        super();
    }

    public init(): void
    {
        var bg: ImgSprite = new ImgSprite(StringUtil.substitute(ResourcePath.bgJpg, "viewbg"+ViewType.SettingView));
        this.addChild(bg);
    }

    public open(): void
    {
    }

    public close(): void
    {

    }
}
