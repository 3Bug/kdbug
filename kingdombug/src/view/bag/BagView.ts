/**
 * 硬盘界面
 * @swyan 
 *
 */
class BagView extends ViewBase
{
    private _bg: egret.Bitmap;
    private _itemPanel: BagItemPanel;

    public constructor()
    {
        super();
    }

    public open(): void
    {
        if(ResLoader.getInstance().hasGroup(ResGroup.BackPackView) == false) {
            GameBus.getInstance().addEventListener(GameEvent.ResGroupLoaded, this.onResGroupCom, this);
            ResLoader.getInstance().loadGroup(ResGroup.BackPackView);
            return;
        }
        this._bg.bitmapData = RES.getRes("bg_jpg");
        this._itemPanel.show();
        
        var obj: Object = new Object();
        obj["id"] = 10000;
        obj["num"] = 1;
        Socket.getInstance().sendProtocol(Protocol.ADD_ITEM,obj);
    }

    public close(): void
    {
        GameBus.getInstance().removeEventListener(GameEvent.ResGroupLoaded, this.onResGroupCom, this);
        this._itemPanel.hide();
    }

    /***资源组**/
    private onResGroupCom(event:GameEvent): void
    {
        GameBus.getInstance().removeEventListener(GameEvent.ResGroupLoaded, this.onResGroupCom, this);
        this.open();
    }
    
    public init(): void
    {
        this._bg = new egret.Bitmap();
        this.addChild(this._bg);
        
        this._itemPanel = new BagItemPanel();
        this._itemPanel.x = 0;
        this._itemPanel.y = 450;
        this.addChild(this._itemPanel);
    }
}
