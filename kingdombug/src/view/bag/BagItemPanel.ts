/**
 * 背包物品容器
 * @swyan 
 *
 */
class BagItemPanel extends PanelBase {
    private _tabBtns: Object[];

    private _bg: egret.Sprite;
    private _tab: TabBar;
    private _allIconArr: ItemIcon[] = null;

    private _itemPanel: egret.Sprite;


    public constructor() {
        super();
    }

    public show(): void {
        this.addAllItems();
        this._tab.addBtns();
        this._tab.setBtnChoose(0);
        this._tab.addEventListener(TabBar.ChangeTab,this.onChangeTab,this);
        GameBus.getInstance().addEventListener(ItemEvent.Init,this.onInit,this);
    }

    public hide(): void {
        GameBus.getInstance().removeEventListener(ItemEvent.Init,this.onInit,this);
        this._tab.removeEventListener(TabBar.ChangeTab,this.onChangeTab,this);
        this._tab.removeBtns();
        this.removeAllItems();
    }

    /***切换页签**/
    private onChangeTab(event: ParamEvent): void {
        this.addAllItems();
    }

    /***初始化物品列表**/
    private onInit(event: ItemEvent): void {
        this.addAllItems();

    }

    /**添加所有物品**/
    private addAllItems(): void {
        this.removeAllItems();
        this._allIconArr = [];
        var items: Object = Bags.getInstance().getItemsByType(this._tab.curIdx);
        var icon: ItemIcon;
        var val: number = 0;
        if(items) {
            var key: any;
            for(key in items) {
                icon = new ItemIcon(items[key]);
                icon.x = (val % 5) * 40;
                icon.y = Math.ceil(val / 5) * 40;
                val++;
                this._itemPanel.addChild(icon);
                this._allIconArr.push(icon);
            }
        }
    }
    
    /**移除所有物品*/
    private removeAllItems(): void {
        if(this._allIconArr == null)
            return;
        for(var i: number = 0;i < this._allIconArr.length;i++) {
            this._allIconArr[i].removeEventListener(egret.TouchEvent.TOUCH_TAP,this.clickIcon,this);
            this._itemPanel.removeChild(this._itemPanel);
            this._allIconArr[i].dispose();
        }
        this._allIconArr = [];
    }

    /***点击图标**/
    private clickIcon(event: egret.TouchEvent): void {

    }
    public init(): void {
        this._bg = new egret.Sprite();
        this._bg.graphics.beginFill(0x000000,0.5);
        this._bg.graphics.drawRoundRect(0,0,Config.stageWid - 40,250,30,30);
        this._bg.graphics.endFill();
        this._bg.x = 20
        this.addChild(this._bg);

        this._tabBtns = [{ lable: "部件",tsize: 16,wid: 60,hei: 40 }
            ,{ lable: "技能",tsize: 16,wid: 60,hei: 40 }
            ,{ lable: "道具",tsize: 16,wid: 60,hei: 40 }]
        this._tab = new TabBar(this._tabBtns);
        this._tab.x = 30;
        this._tab.y = -40;
        this.addChild(this._tab);

        this._itemPanel = new egret.Sprite();
        this._itemPanel.x = 10;
        this._itemPanel.y = 10;
        this.addChild(this._itemPanel);
    }
}
