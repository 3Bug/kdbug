/**
 * 图标
 * @swyan 
 *
 */
class ItemIcon extends egret.Sprite {
    private _data: ItemData;
    private _pic: ImgSprite;

    public constructor(data: ItemData) {
        super();
        this._data = data;

        this._pic = new ImgSprite(StringUtil.substitute(ResourcePath.Icon,this._data.id));
        this.addChild(this._pic);
    }

    public dispose(): void {
        this._pic.despose();
        this._pic = null;
        this._data = null;
    }

    public get data(): ItemData {
        return this._data;
    }
}
