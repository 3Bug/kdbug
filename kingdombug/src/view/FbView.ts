/**
 * 副本界面
 * @author 
 *
 */
class FbView extends ViewBase
{
    public constructor()
    {
        super();
    }

    public init(): void
    {
        var bg: ImgSprite = new ImgSprite(StringUtil.substitute(ResourcePath.bgJpg, "viewbg"+ViewType.FightView));
        this.addChild( bg );
        
    }

    public open(): void
    {
        /**测试PVP战斗**/
        var data: Object = new Object();
        data["playerid1"] = "41794E505852554B6645704E49314A48544578596567355141464A5441516B4541516741424163434267464D5445774A415159475445784D42415A3043516830416751466351634A64514A7A42676C314158514A645849486441494163774D486467467662304642";
        data["playerid2"] = "42794A43556B4E63664277514A4635474530305063413157417752534267514642314941556C5A54426C524D5445774A415159475445784D6467454943514D434151594443514A3163774D46636E454463584D464148523163775147645141474267467662304642";
        Socket.getInstance().sendProtocol(Protocol.START_BATTLE,data);
    }

    public close(): void
    {

    }
}
