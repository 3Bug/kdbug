/**
 * 背景容器
 * @swyan 
 *
 */
class SceneBgPanel extends PanelBase
{
    private _bg: ImgSprite;
    private _loadElementCom: boolean = false;
    private _shake: number = 0;
    private _ball1: egret.Bitmap;
    private _ball2: egret.Bitmap;
    private _ball3: egret.Bitmap;
    private _ball4: egret.Bitmap;
    private _gang: egret.Bitmap;

    public constructor()
    {
        super();
        this._loadElementCom = false;
        this._bg = new ImgSprite();
        //this._bg.y = 375;
        this.addChild( this._bg );
        this._bg.addEventListener( ImgSprite.LoadCom, this.bgCom, this );
        this._bg.setImg( StringUtil.substitute( ResourcePath.bgJpg, "scene" ) );

        this._ball1 = new egret.Bitmap();
        this._ball1.scaleX = 0.4;
        this._ball1.scaleY = 0.4;
        this._ball1.x = -10;
        this._ball1.y = 470;
        this.addChild( this._ball1 );

        this._ball2 = new egret.Bitmap();
        this._ball2.scaleX = 0.9;
        this._ball2.scaleY = 0.9;
        this._ball2.x = 130;
        this._ball2.y = 480;
        this.addChild( this._ball2 );

        this._ball3 = new egret.Bitmap();
        this._ball3.scaleX = 1.3;
        this._ball3.scaleY = 1.3;
        this._ball3.x = 300;
        this._ball3.y = 600;
        this.addChild( this._ball3 );

        this._ball4 = new egret.Bitmap();
        this._ball4.scaleX = 0.5;
        this._ball4.scaleY = 0.5;
        this._ball4.x = 370;
        this._ball4.y = 430;
        this.addChild( this._ball4 );

        this._gang = new egret.Bitmap();
        this._gang.x = 0;
        this._gang.y = 397;
        this.addChild( this._gang );
        
    }

    public show(): void
    {
        this._shake = 0;
        TimerMgr.getInstance().add( TimerMgr.t100, this.timer, this );
        this.tweenBall();
    }

    public hide(): void
    {
        egret.Tween.removeTweens( this._ball1 );
        egret.Tween.removeTweens( this._ball2 );
        egret.Tween.removeTweens( this._ball3 );
        egret.Tween.removeTweens( this._ball4 );
        TimerMgr.getInstance().remove( this.timer );
        //this._bg.despose();
    }

    private timer(): void
    {
        if ( this._loadElementCom && this._shake == 0 )
        {
            if ( Math.round( Math.random() * 50 ) == 0 )
            {
                //开始抖动
                this._shake = 2;
            }
        }
        if ( this._shake > 5 )
        {
            this._bg.setImg( StringUtil.substitute( ResourcePath.bgJpg, "scene" ) );
            this._shake = 0;
            return;
        }
        if ( this._shake > 0 )
        {
            this._bg.setTexture( StringUtil.substitute( "shake{0}_jpg", this._shake % 2 + 1 ) );
            this._shake++;
        }

    }

    private onSceneComplete( event: RES.ResourceEvent ): void
    {
        if ( event.groupName == "sceneElement" )
        {
            this._loadElementCom = true;
            RES.removeEventListener( RES.ResourceEvent.GROUP_COMPLETE, this.onSceneComplete, this );
            this._ball1.texture = RES.getRes( "ball_png" );
            this._ball2.texture = RES.getRes( "ball_png" );
            this._ball3.texture = RES.getRes( "ball_png" );
            this._ball4.texture = RES.getRes( "ball_png" );
            this._gang.texture = RES.getRes( "gang_png" );
            this.tweenBall();
        }
    }

    private tweenBall(): void
    {
        this._ball1.x = -10;
        this._ball1.y = 470;
        this._ball2.x = 130;
        this._ball2.y = 480;
        this._ball3.x = 300;
        this._ball3.y = 600;
        this._ball4.x = 370;
        this._ball4.y = 430;
        this._ball1.scaleX = 0.4;
        this._ball1.scaleY = 0.4;
        this._ball2.scaleX = 0.9;
        this._ball2.scaleY = 0.9;
        this._ball3.scaleX = 1.3;
        this._ball3.scaleY = 1.3;
        this._ball4.scaleX = 0.5;
        this._ball4.scaleY = 0.5;
        if ( this._ball1.texture != null )
            egret.Tween.get( this._ball1, { loop: true }).to( { x: 200, y: 410, scaleX: 0.2, scaleY: 0.2 }, 6000 ).to( { x: -10, y: 470, scaleX: 0.4, scaleY: 0.4 }, 6000 );
        if ( this._ball2.texture != null )
            egret.Tween.get( this._ball2, { loop: true }).to( { y: 600 }, 3000 ).to( { y: 480 }, 3000 );
        if ( this._ball3.texture != null )
            egret.Tween.get( this._ball3, { loop: true }).to( { y: 680 }, 4000 ).to( { y: 600 }, 4000 );
        if ( this._ball4.texture != null )
            egret.Tween.get( this._ball4, { loop: true }).to( { y: 500, scaleX: 0.4, scaleY: 0.4 }, 3000 ).to( { y: 430, scaleX: 0.5, scaleY: 0.5 }, 3000 );
    }
    /***背景加载完成**/
    private bgCom(): void
    {
        this._bg.removeEventListener( ImgSprite.LoadCom, this.bgCom, this );

        RES.addEventListener( RES.ResourceEvent.GROUP_COMPLETE, this.onSceneComplete, this );
        RES.loadGroup( "sceneElement" );
    }
}
