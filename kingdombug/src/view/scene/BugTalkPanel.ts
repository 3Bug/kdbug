/**
 * BUG说话容器
 * @swyan 
 *
 */
class BugTalkPanel extends egret.Sprite
{
    private _bg: egret.Sprite;
    private _text: egret.TextField;
    private _wid: number;

    public constructor( wid: number = 150 )
    {
        super();
        this._wid = wid;
        this.initView();
    }

    /***内容**/
    public say( str: string ): void
    {
        this._text.text = str;
        this._bg.graphics.clear();

        this._bg.graphics.beginFill( 0x000000, 0.7 );
        this._bg.graphics.drawRoundRect( 0, 0, this._text.width + 20, this._text.height + 20, 10 );
        this._bg.graphics.endFill();

        this._text.x = 10;
        this._text.y = 10;
    }

    private initView(): void
    {
        this._bg = new egret.Sprite();
        this._bg.x = 0;
        this._bg.y = 0;
        this.addChild( this._bg );

        this._text = new egret.TextField();
        this._text.lineSpacing = 3;
        this._text.size = 20;
        this._text.width = this._wid;
        this._text.x = 0;
        this._text.y = 0;
        this.addChild( this._text );
    }
}
