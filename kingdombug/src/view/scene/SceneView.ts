/**
 * 场景界面
 * @swyan 
 *
 */
class SceneView extends ViewBase
{
    private _bg: SceneBgPanel;
    private _bugArea: BugArea;

    public constructor()
    {
        super();
    }

    public init(): void
    {
        this._bg = new SceneBgPanel();
        this.addChild(this._bg);
        
        this._bugArea = new BugArea();
        this._bugArea.x = 0;
        this._bugArea.y = 400;
        this.addChild( this._bugArea );
        
        //egret.Profiler.getInstance().run();
    }


    public open(): void
    {
        this._bg.show();
        this._bugArea.show();
    }

    public close(): void
    {
        this._bg.hide();
        this._bugArea.hide();
    }
}
