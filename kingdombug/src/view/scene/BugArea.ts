/**
 * bug活动区域
 * @swyan 
 *
 */
class BugArea extends PanelBase
{
    private _bg: egret.Sprite;
    private _wid: number;
    private _hei: number;
    private _bug: Bug;
    private _times: number;
    private _talk: BugTalkPanel;
    private _isWalking: Boolean = false;

    public constructor( wid: number = 480, hei: number = 340 )
    {
        super();
        this._wid = wid;
        this._hei = hei;
        this.initView();
    }


    public show(): void
    {
        GameBus.getInstance().addEventListener( PlayerEvent.Update, this.updateBugData, this );
        this._isWalking = false;
        this._bug.playAct( "stay1" );
        this._times = 0;
        TimerMgr.getInstance().add( TimerMgr.t50, this.onTimer, this );
        this._bug.touchEnabled = true;
        this._bug.addEventListener( egret.TouchEvent.TOUCH_TAP, this.bugClick, this );
        this._bg.touchEnabled = true;
        this._bg.addEventListener( egret.TouchEvent.TOUCH_TAP, this.bugWalk, this );
    }

    public hide(): void
    {
        GameBus.getInstance().removeEventListener( PlayerEvent.Update, this.updateBugData, this );
        this._bug.touchEnabled = false;
        this._bug.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.bugClick, this );
        this._bg.touchEnabled = false;
        this._bg.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.bugWalk, this );
        egret.Tween.removeTweens( this._bug );
        TimerMgr.getInstance().remove( this.overTalk );
        TimerMgr.getInstance().remove( this.onTimer );
        this._bug.stopAct();
    }
    
    /**更新bug数据**/
    private updateBugData(): void
    {
        if ( this._bug )
            this._bug.setLooksByBugBase(Players.getInstance().bug);
    }
    
    /**主动点击BUG*/
    private bugClick( event: egret.TouchEvent ): void
    {
        event.stopPropagation();
        egret.Tween.removeTweens( this._bug );
        this._times = 1;//重置 计时
        this._isWalking = false;
        this.playAct();
    }
    /**bug主动走路*/
    private bugWalk( event: egret.TouchEvent ): void
    {
        egret.Tween.removeTweens( this._bug );
        this._isWalking = true;
        if ( this._bug.curPlayingAct != "walk" )
            this._bug.playAct( "walk" );

        var p: egret.Point = this.globalToLocal( event.stageX, event.stageY );
        var valX: number = Math.round( Math.abs( this._bug.x - p.x ) );
        var valY: number = Math.round( Math.abs( this._bug.y - p.y ) );

        if ( p.x < this._bug.x )
            this._bug.scaleX = Math.abs( this._bug.scaleX ) * -1;
        else
            this._bug.scaleX = Math.abs( this._bug.scaleX );

        var time: number = Math.round( Math.sqrt( valX * valX + valY * valY ) / 0.12 );
        egret.Tween.get( this._bug ).to( { x: p.x, y: p.y }, time ).call( this.walkCom, this );
    }

    private walkCom(): void
    {
        this._isWalking = false;
        egret.Tween.removeTweens( this._bug );
        this._bug.playAct( "stay1" );

    }

    /***定时器**/
    private onTimer(): void
    {
        this._times++;
        if ( this._times % 100 == 0 && Math.round( Math.random() ) == 1 )
        {
            this.playAct();
        }

        if ( this._talk.visible )
            this.updateTalkPos();

        /***校对bug的大小*/
        if ( this._bug.scaleX > 0 )
            this._bug.scaleX = 1 + 0.2 * ( this._bug.y / 340 );
        else
            this._bug.scaleX = ( 1 + 0.2 * ( this._bug.y / 340 ) ) * -1;
        this._bug.scaleY = 1 + 0.2 * ( this._bug.y / 340 );
    }

    private playAct( canMove: boolean = true ): void
    {
        var data: AutoActData = AutoActs.getInstance().getAct();
        if ( data )
        {
            if ( data.act == "talk" )
            {
                TimerMgr.getInstance().remove( this.overTalk );
                this._talk.visible = true;
                this._talk.say( AutoActXX.talkArr[Math.round( Math.random() * ( AutoActXX.talkArr.length - 1 ) )] );
                this.updateTalkPos();
                TimerMgr.getInstance().add( 4000, this.overTalk, this );
                return;
            }
            if ( this._isWalking == true )
                return;//主动走路时 只会触发说话动作
            this._bug.playAct( data.act, data.times, this.playActCom, this );
            if ( data.isMove )
            {
                var _x: number = Math.round( Math.random() * ( this._wid - 40 ) ) + 20;
                var _y: number = Math.round( Math.random() * ( this._hei - 40 ) ) + 20;
                if ( _x < this._bug.x )
                    this._bug.scaleX = Math.abs( this._bug.scaleX ) * -1;
                else
                    this._bug.scaleX = Math.abs( this._bug.scaleX );
                egret.Tween.get( this._bug ).to( { x: _x, y: _y }, 4000 );
            }
        }
    }

    private updateTalkPos(): void
    {
        if ( ( this._talk.width + this._bug.x + 30 ) > this._wid )
            this._talk.x = this._bug.x - 30 - this._talk.width;
        else
            this._talk.x = this._bug.x + 30;

        this._talk.y = this._bug.y - 200;
    }

    private overTalk(): void
    {
        TimerMgr.getInstance().remove( this.overTalk );
        this._talk.visible = false;
    }

    private playActCom(): void
    {
        this._bug.playAct( "stay1" );
    }

    /***初始化界面**/
    private initView(): void
    {
        this._bg = new egret.Sprite();
        this._bg.graphics.beginFill( 0xff0000, 0 );
        this._bg.graphics.drawRect( 0, 0, this._wid, this._hei );
        this._bg.graphics.endFill();
        this.addChild( this._bg );

        this._bug = new Bug();
        this._bug.x = 200;
        this._bug.y = 250;
        this.addChild( this._bug );

        this._talk = new BugTalkPanel();
        this._talk.visible = false;
        this.addChild( this._talk );
    }
}
