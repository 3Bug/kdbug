/**
 * 界面类型
 * @swyan 
 *
 */
class ViewType {
        
    /**场景界面**/
    public static SceneView: number = 1;
    
    /**角色界面**/
    public static RoleView: number = 2;
    
    /**硬盘界面**/
    public static BagView: number = 3;
    
    /**战斗界面**/
    public static FightView: number = 4;
    
    /**设置界面**/
    public static SettingView: number = 5;
    
	public constructor() {
	}
}
