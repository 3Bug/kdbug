/**
 * 界面基类
 * @swyan 
 *
 */
class ViewBase extends egret.Sprite{
    
    protected  _bInited:Boolean = false; // 是否已初始化
    protected  _bOpened:Boolean = false; // 是否已打开
    
	public constructor() {
        super();
        this.init();
	}
	
    /**
    * 初始化，实例化后调用,只在实例化后调用一次
    */
    public  init():void
    {
        this._bInited = true;
    }
    
    /**
    * 关闭，从舞台上remove掉后调用，每次从舞台上remove掉的时候都调用
    */
    public  close():void
    {
        this._bOpened = false;
    }
    
    /**
    * 打开，加入到舞台上后调用，每次在舞台上显示时都调用
    */
    public  open():void
    {
        this._bOpened = true;
    }
    
}
