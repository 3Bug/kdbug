/**
 * 显示容器基类
 * @swyan 
 *
 */
class PanelBase extends egret.Sprite{
    protected  _bInited:Boolean = false; // 是否已初始化
    protected  _bShowed:Boolean = false; // 是否已打开
    
	public constructor() {
        super();
        this.init();
	}
	
    /**
    * 初始化，实例化后调用,只在实例化后调用一次
    */
    public  init():void
    {
        this._bInited = true;
    }
    
    /**
    * 显示
    */
    public  show():void
    {
        this._bShowed = true;
    }
    
    /**
    * 隐藏
    */
    public  hide():void
    {
        this._bShowed = false;
    }
}
