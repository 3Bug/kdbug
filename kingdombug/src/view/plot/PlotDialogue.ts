/**
 * 剧情对话容器
 * @swyan 
 *
 */
class PlotDialogue extends PanelBase
{
    /**剧情数据*/
    private _plotArr: PlotData[];
    
    /**对话容器**/
    private _panel: DialoguePanel;

    public constructor( arr: PlotData[] )
    {
        super();
        this._plotArr = arr.concat();
        this.initView();
    }

    public show(): void
    {
        this._panel.visible = true;
        this.touchEnabled = true;
        this.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onClick, this );
        this.updatePlot();
    }

    public hide(): void
    {
        this.touchEnabled = false;
        this.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.onClick, this );
    }

    /**点击剧情**/
    private onClick( event: egret.TouchEvent ): void
    {
        this.updatePlot();
    }
    
    /**更新剧情**/
    private updatePlot():void{
        if ( this._plotArr.length <= 0 )
        {
            GameBus.getInstance().dispatchEvent( new PlotEvent( PlotEvent.PlayPlotOver, this ) );
            return;
        }
        var data: PlotData = this._plotArr.shift();
        this._panel.update(data);
    }
    
    /**初始化界面**/
    private initView():void{
        this._panel = new DialoguePanel();
        this._panel.visible = false;
        this._panel.x = 50;
        this._panel.y = 600;
        this.addChild(this._panel);
    }
}
