/**
 * 对话容器
 * @swyan 
 *
 */
class DialoguePanel extends egret.Sprite
{
    private _plotData: PlotData;
    private _head: ImgSprite;
    private _bg: egret.Sprite;
    private _desc: egret.TextField;

    public constructor()
    {
        super();
        this.init();
    }
	
    /***更新数据**/
    public update( p: PlotData ): void
    {
        this._plotData = p;
        if ( this._plotData == null )
            return;
        this._head.setImg( StringUtil.substitute( ResourcePath.plotHead, p.roleType ) );
        this.onHeadCom();

        this._desc.text = p.desc;
        if ( p.pos == 1 )
            this._desc.x = 60;
        else
            this._desc.x = 20;
    }
	
    /**销毁资源**/
    public dispose(): void
    {
        this._bg.graphics.clear();
        this._head.removeEventListener( ImgSprite.LoadCom, this.onHeadCom, this );
        this._head.despose();
    }

    private init(): void
    {
        this._bg = new egret.Sprite();
        this._bg.graphics.beginFill( 0x000000, 0.9 );
        this._bg.graphics.drawRoundRect(0, 0, Config.stageWid - 100, 130,30);
        this._bg.graphics.endFill();
        this.addChild( this._bg );

        this._desc = new egret.TextField();
        this._desc.size = 20;
        this._desc.y = 20;
        this.addChild( this._desc );

        this._head = new ImgSprite();
        this.addChild( this._head );
        this._head.addEventListener( ImgSprite.LoadCom, this.onHeadCom, this );
    }

    private onHeadCom( event?: egret.Event ): void
    {
        if ( this._plotData )
        {
            this._head.y = 0 - ( this._head.height >> 1 );
            if ( this._plotData.pos == 1 )
            {
                this._head.x = 0 - ( this._head.width >> 1 );
                this._head.scaleX = 1;
            }
            else
            {
                this._head.x = this._bg.width + ( this._head.width >> 1 );
                this._head.scaleX = -1;
            }
        }
    }
}
