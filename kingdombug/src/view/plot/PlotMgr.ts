/**
 * 剧情控制器
 * @swyan 
 *
 */
class PlotMgr
{
    private static _instance: PlotMgr;
    private static _allowInstance: Boolean;
    public constructor()
    {
        if ( !PlotMgr._allowInstance )
            return;
        GameBus.getInstance().addEventListener( PlayerEvent.BugLvUp, this.onBugLvUp, this );
        GameBus.getInstance().addEventListener( PlotEvent.PlayPlotOver, this.onPlayPlotOver, this );
    }

    /***bug等级提升**/
    private onBugLvUp( event: PlayerEvent ): void
    {
        this.checkPlot( 1 );
    }
    
    /***检测剧情是否触发    trigger  1等级    具体请看 plot_data**/
    public checkPlot( trigger: number ): void
    {
        var key: number = 9332;
        switch ( trigger )
        {
            case 1:
                key = Players.getInstance().bug.lv;
                break;
        }
        this.playPlot( Plots.getInstance().getPlot( trigger, key ) );
    }

    /***播放剧情**/
    public playPlot( arr: PlotData[] ): void
    {
        if ( arr == null || arr.length < 1 )
            return;
        var p: PlotData = arr[0];
        var panel: PanelBase;
        if ( p.plotType == 1 )
            panel = new PlotBlack( arr );
        else
            panel = new PlotDialogue( arr );

        var layer: egret.Sprite = GameLayer.getInstance().plotLayer;
        var temp: PanelBase;
        while ( layer.numChildren > 0 )
        {
            temp = <PanelBase>layer.getChildAt( 0 );
            if ( temp )
                temp.hide();
            layer.removeChildAt( 0 );
        }

        panel.show();
        layer.addChild( panel );
    }
    
    /***播放剧情完毕**/
    private onPlayPlotOver( event: PlotEvent ): void
    {
        var temp: PanelBase;
        var layer: egret.Sprite = GameLayer.getInstance().plotLayer;
        var len: number = layer.numChildren;
        for ( var i: number = 0; i < len; i++ )
        {
            temp = <PanelBase>layer.getChildAt( i );
            if ( temp && temp == event.data )
            {
                temp.hide();
                layer.removeChildAt( 0 );
            }
        }
    }

    public static getInstance(): PlotMgr
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new PlotMgr();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
