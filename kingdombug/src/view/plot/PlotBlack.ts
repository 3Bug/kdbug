/**
 * 黑幕剧情容器
 * @swyan
 *
 */
class PlotBlack extends PanelBase
{
    
    /**剧情数据*/
    private _plotArr: PlotData[];

    /**背景*/
    private _bg: egret.Sprite;
    
    /**跳过剧情**/
    private _skipText: egret.TextField;

    private _content: egret.Sprite;

    private _valY: number;

    public constructor( arr: PlotData[] )
    {
        super();
        this._plotArr = arr.concat();
        if ( this._plotArr )
            this.initView();
    }

    public show(): void
    {
        this._valY = 0;
        TimerMgr.getInstance().add( TimerMgr.t1000, this.addDesc, this );
    }

    public hide(): void
    {
        TimerMgr.getInstance().remove( this.addDesc );
        this._skipText.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.onSkip, this._skipText );
        this._bg.graphics.clear();

        while ( this.numChildren > 0 )
            this.removeChildAt( 0 );
    }
    
    /**添加描述**/
    private addDesc(): void
    {
        if ( this._plotArr.length <= 0 )
        {
            GameBus.getInstance().dispatchEvent( new PlotEvent( PlotEvent.PlayPlotOver, this ) );
            return;
        }

        var data: PlotData = this._plotArr.shift();
        var text: egret.TextField = new egret.TextField();
        text.text = data.desc;
        text.x = ( this._bg.width - text.width ) >> 1;
        text.y = this._valY;
        this._content.addChild( text );
        this._valY += text.height + 5;
        egret.Tween.get( this._content ).to( { y: ( ( this._bg.height - this._content.height ) >> 1 ) - 50 }, 400 );
    }
	
    /**初始化界面**/
    public initView(): void
    {
        this._bg = new egret.Sprite();
        this._bg.graphics.beginFill( 0x000000 );
        this._bg.graphics.drawRect( 0, 0, Config.stageWid, Config.stageHei );
        this._bg.graphics.endFill();
        this.addChild( this._bg );

        this._content = new egret.Sprite();
        this._content.y = ( this._bg.height >> 1 ) - 50;
        this.addChild( this._content );

        this._skipText = new egret.TextField();
        this._skipText.textFlow = new egret.HtmlTextParser().parser( "<font color = '0xffff00'>跳过剧情</font>" );
        this._skipText.touchEnabled = true;
        this._skipText.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onSkip, this );
        this._skipText.y = 770;
        this.addChild( this._skipText );
    }
    
    /**跳过剧情**/
    private onSkip( event: egret.TouchEvent ): void
    {
        GameBus.getInstance().dispatchEvent( new PlotEvent( PlotEvent.PlayPlotOver, this ) );
        event.stopPropagation();
    }
}
