/**
 * 角色界面
 * @swyan
 *
 */
class RoleView extends ViewBase
{
    private _bg: ImgSprite;

    public constructor()
    {
        super();
    }

    public init(): void
    {
        this._bg = new ImgSprite();
        this.addChild( this._bg );
        this._bg.setImg( StringUtil.substitute( ResourcePath.bgJpg, "viewbg" + ViewType.RoleView ) );
    }

    public open(): void
    {
    }

    public close(): void
    {

    }
}
