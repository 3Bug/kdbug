/**
 * 登录界面
 * @swyan 
 *
 */
class LoginView extends egret.Sprite
{
    private _bg: egret.Bitmap;
    private _btn: CustomButton;

    public constructor()
    {
        super();
        this.createView();
    }

    private createView(): void
    {
        this.graphics.beginFill(0xffffff);
        this.graphics.drawRect(0,0,Config.stageWid,Config.stageHei);
        this.graphics.endFill();
        
        this._bg = new egret.Bitmap( RES.getRes( "loginBg_png" ) );
        this._bg.x = ( Config.stageWid - this._bg.width ) >> 1;
        this._bg.y = 100;
        this.addChild( this._bg );
        egret.Tween.get( this._bg, { loop: true }).to( { y: 150 }, 1000, egret.Ease.backInOut ).to( { y: 100 }, 1000, egret.Ease.backInOut );

        this._btn = new CustomButton();
        this._btn.setPic( "loginBtn_png", "loginBtn_png" );
        this._btn.x = 150;
        this._btn.y = 500;
        this._btn.touchEnabled = true;
        this._btn.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onClick, this );
        this.addChild( this._btn );
    }
    private onClick( event: egret.TouchEvent ): void
    {
        Login.getInstance().checkLogin();
    }

    public dispose(): void
    {
        this._btn.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.onClick, this );
        egret.Tween.removeTweens( this._bg );
        this._bg.texture = null;
        this._btn.dispose();
    }
}
