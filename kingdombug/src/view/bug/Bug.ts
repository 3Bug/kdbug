/**
 * Bug
 * @swyan 
 *
 */
class Bug extends egret.Sprite
{
    private _mc: DragonBonesMc;
    
    /***面向  1侧面**/
    public direction: number = 1;
    
    /**点击播放动作**/
    public clickPlayAct: boolean = false;

    private _times: number;

    private _callBackFun: Function;

    private _callBackTar: any;

    public curPlayingAct: string = "";
    
    /***面向  1侧面**/
    public constructor( direction: number = 1, clickPlayAct: boolean = false )
    {
        super();
        this._mc = new DragonBonesMc( "kingdombug_json", "texture_json", "texture_png", "bug" );
        this._mc.scaleX = 0.6;
        this._mc.scaleY = 0.6;
        this._mc.x = 0;
        this._mc.y = 0;
        this.addChild( this._mc );

        this.clickPlayAct = clickPlayAct;

        if ( this.clickPlayAct )
        {
            this.touchEnabled = true;
            this.addEventListener( egret.TouchEvent.TOUCH_TAP, this.clickBug, this );
        }

    }

    /***设置某部分 外观**/
    public setLook( lookType: number, lookVal: number ): void
    {
        var boneName: string = BugLooksType.getNameKey( lookType );
        var textureName: string = boneName + "_" + this.direction + "_" + lookVal.toString();
        if ( this._mc )
            this._mc.setLook( boneName, textureName );
    }
    
    /**播放动作**/
    public playAct( act: string, times: number = -1, callBackFun: Function = null, callBackTar: any = null ): void
    {
        if ( callBackFun != null && callBackTar != null )
        {
            this._callBackFun = callBackFun;
            this._callBackTar = callBackTar;
            this._mc.addEventListener( DragonBonesMc.playActCom, this.playActCom, this );
        }
        this.curPlayingAct = act;
        if ( this._mc && act != "" )
            this._mc.play( act, times );

    }
    
    /**停止动作**/
    public stopAct(): void
    {
        if ( this._mc );
        this._mc.stop();
    }

    /**点击播放动画**/
    private clickBug( event: egret.TouchEvent ): void
    {
        var data: AutoActData = AutoActs.getInstance().getAct();
        if ( data && data.act != "talk" )
        {
            this.playAct( data.act );
        }

    }
    
    /***播放动作完成*/
    private playActCom(): void
    {
        //this._mc.removeEventListener( DragonBonesMc.playActCom, this.playActCom, this );
        if ( this._callBackFun )
            this._callBackFun.call( this._callBackTar );
    }
    	
    /**设置根据BUG基本数据外观**/
    public setLooksByBugBase( bug: BugBase ): void
    {
        if ( bug == null )
            return;
        this.setLook( BugLooksType.head, bug.head );
        this.setLook( BugLooksType.body, bug.body );
        this.setLook( BugLooksType.armUpperL, bug.armUpperL );
        this.setLook( BugLooksType.armUpperR, bug.armUpperR );
        this.setLook( BugLooksType.armLowerL, bug.armLowerL );
        this.setLook( BugLooksType.armLowerR, bug.armLowerR );
        this.setLook( BugLooksType.handL, bug.handL );
        this.setLook( BugLooksType.handR, bug.handR );
        this.setLook( BugLooksType.legUpperL, bug.legUpperL );
        this.setLook( BugLooksType.legUpperR, bug.legUpperR );
        this.setLook( BugLooksType.legLowerL, bug.legLowerL );
        this.setLook( BugLooksType.legLowerR, bug.legLowerR );
        this.setLook( BugLooksType.footL, bug.footL );
        this.setLook( BugLooksType.footR, bug.footR );
        this.setLook( BugLooksType.face, bug.face );
    }
    
    /**还原BUG基本外观**/
    public resetBugBaseLooks(): void
    {
        this.setLook( BugLooksType.head, 1 );
        this.setLook( BugLooksType.body, 1 );
        this.setLook( BugLooksType.armUpperL, 1 );
        this.setLook( BugLooksType.armUpperR, 1 );
        this.setLook( BugLooksType.armLowerL, 1 );
        this.setLook( BugLooksType.armLowerR, 1 );
        this.setLook( BugLooksType.handL, 1 );
        this.setLook( BugLooksType.handR, 1 );
        this.setLook( BugLooksType.legUpperL, 1 );
        this.setLook( BugLooksType.legUpperR, 1 );
        this.setLook( BugLooksType.legLowerL, 1 );
        this.setLook( BugLooksType.legLowerR, 1 );
        this.setLook( BugLooksType.footL, 1 );
        this.setLook( BugLooksType.footR, 1 );
        this.setLook( BugLooksType.face, 1 );
    }

    public dispose(): void
    {

        if ( this.clickPlayAct )
            this.removeEventListener( egret.TouchEvent.TOUCH_TAP, this.clickBug, this );
        this._mc.despose();

    }
}
