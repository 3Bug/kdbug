/**
 * 战斗者容器
 * @swyan 
 *
 */
class Fighter extends egret.Sprite
{
    
    /**类型**/
    private _type: number;
    
    /**bug**/
    private _bug: Bug;
    
    /***type 类型  1BUG  2怪物**/
    public constructor( type: number )
    {
        super();
        this._type = type;
        if ( this._type == 1 )
            this.initBug();
        else if ( this._type == 2 )
            this.initMonster();
    }
    
    /***播放动作   key动作关键值,isAct是否攻击动作 (false为受击动作)**/
    public playAct( key: number, isAct: boolean = true ): void
    {
        var str: string = "";
        if ( isAct )
            str = "act" + key;
        else
            str = "inact" + key;
        if ( this._bug )
            this._bug.playAct( str, 1 );
    }

    /**初始化bug元件*/
    private initBug(): void
    {
        this._bug = new Bug();
        this.addChild( this._bug );
        this._bug.playAct( "stay1" );
    }

    /***初始化怪物**/
    private initMonster(): void
    {

    }

    /***type 类型  1BUG  2怪物**/
    public get type(): number
    {
        return this._type;
    }
}
