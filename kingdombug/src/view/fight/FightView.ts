
/**
 * 战斗界面
 * @swyan 
 *
 */
class FightView extends egret.Sprite
{
    private _bg: ImgSprite;
    private _closeBtn: egret.TextField;
    private _fight: FightPanel;

    public constructor()
    {
        super();
        this.init();
    }

    public init(): void
    {
        this._bg = new ImgSprite();
        this.addChild( this._bg );

        this._fight = new FightPanel();
        this._fight.x = 0;
        this._fight.y = 200;
        this.addChild( this._fight );

        this._closeBtn = new egret.TextField();
        this._closeBtn.textColor = 0x000000;
        this._closeBtn.text = "跳过战斗";
        this._closeBtn.x = 320;
        this._closeBtn.y = 20;
        this.addChild( this._closeBtn );
        this._closeBtn.touchEnabled = true;
        this._closeBtn.addEventListener( egret.TouchEvent.TOUCH_TAP, this.onSkip, this );
    }

    /***battleData 战斗数据   type战斗类型 1pvp 2pve**/
    public open( battleData: any[],type:number): void
    {
        this._bg.setImg( StringUtil.substitute( ResourcePath.bgJpg, "fight" ) );
        this._fight.initFight(type,battleData);
    }

    public hide(): void
    {
        this._fight.dispose();
        this._bg.despose();
    }

    private onSkip(): void
    {
        GameBus.getInstance().dispatchEvent( new FightEvent( FightEvent.Over ) );
    }
}
