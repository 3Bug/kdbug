/**
 * 战斗容器
 * @swyan 
 *
 */
class FightPanel extends egret.Sprite
{
    /**战斗类型   1pvp  2pve**/
    private _type: number = 1;
    
    /**战斗数据**/
    private _battleData: any[];
    
    /**回合数**/
    private _round: number;
    
    /**总回合数**/
    private _totalRound: number;
    
    /**回合数据**/
    private _roundData: Object;
    
    /**自己**/
    private _mine: Fighter;
    
    /**敌人**/
    private _enemy: Fighter;
    
    /**运算次数**/
    private _times: number = 0;
    
    /**我的技能数据**/
    private _mineSkillData: SkillData;
    
    /***敌人的技能数据**/
    private _enemySkillData: SkillData;
    
    /**我的技能播放时间**/
    private _myPlayActTime: number = 0;
    
    /**敌人的技能播放时间**/
    private _enemyPlayActTime: number = 0;
    
    /**我的回合数据**/
    private _mineRoundData: Object;
    
    /**敌人的回合数据**/
    private _enemyRoundData: Object;
    
    /**该回合总时间**/
    private _totalTimes: number;
    
    /**受击时间**/
    private _inActTime: number;

    private _valX: number;
    private _valY: number;

    public constructor()
    {
        super();
    }

    /**type战斗类型 1pvp 2pve**/
    public initFight( type: number, battleData: any[] ): void
    {
        this._battleData = battleData;
        this._type = type;

        this.dispose();

        if ( this._battleData == null )
            return;
        this._roundData = new Object();
        this._totalRound = this._battleData.length;
        for ( var i: number = 0; i < this._battleData.length; i++ )
        {
            this._roundData[this._battleData[i].roundNum + ""] = <any[]>this._battleData[i].roundData;
        }

        this._enemy = new Fighter( type );
        this._enemy.x = 400;
        this._enemy.y = 300;
        this._enemy.scaleX = -1;
        this.addChild( this._enemy );

        this._mine = new Fighter( 1 );
        this._mine.x = 100;
        this._mine.y = 300;
        this.addChild( this._mine );

        this.startFight();
    }

    /**开始战斗**/
    private startFight(): void
    {
        if ( this._battleData == null )
            return;
        this._totalTimes = 0;
        this._times = 0;
        this._round = 0;
        this._mineRoundData = new Object();
        this._enemyRoundData = new Object();
        TimerMgr.getInstance().add( TimerMgr.t100, this.step, this );
    }

    private step(): void
    {
        if ( this._round > this._totalRound )
        {
            this.overFight();
            return;
        }

        if ( this._totalTimes == 0 || this._times >= this._totalTimes )
        {//到下一个回合
            this.nextRound();
            this._mine.scaleX = this._mine.x < this._enemy.x ? 1 : -1;
            this._enemy.scaleX = -1 * this._mine.scaleX;
        }
        
        /**我的技能播放时间**/
        if ( this._myPlayActTime == this._times )
            this._mine.playAct( this._mineSkillData.act );
        
        /**敌人的技能播放时间**/
        if ( this._enemyPlayActTime == this._times )
            this._enemy.playAct( this._enemySkillData.act );
        
        /**移动战斗者**/
        this.moveFighter();
        
        /**播放受击动作**/
        if ( this._inActTime == this._times )
            this.playInact();

        this._times++;
    }

    /***结束战斗**/
    private overFight(): void
    {
        TimerMgr.getInstance().remove( this.step );
    }
    
    /**播放受击动作*/
    private playInact(): void
    {
        this._mine.playAct( this._enemySkillData.inact, false );
        this._enemy.playAct( this._mineSkillData.inact, false );
        this.inactMove( this._mine, this._mineSkillData.inactMove, this._mineSkillData.inactTime * 100 );
        this.inactMove( this._enemy, this._enemySkillData.inactMove, this._enemySkillData.inactTime * 100 );
    }
    
    /**受击移动**/
    private inactMove( trage: Fighter, type: number, time: number ): void
    {
        switch ( type )
        {
            case 0:
                return;
            case 1:
                egret.Tween.get( trage ).to( { x: trage.x + trage.scaleX * -1 * 100 }, time );
                return;
            case 2:
                egret.Tween.get( trage ).to( { x: trage.x + trage.scaleX * -1 * 100, y: trage.y + 40 }, time );
                return;
            case 3:
                egret.Tween.get( trage ).to( { x: trage.x + trage.scaleX * -1 * 100, y: trage.y - 40 }, time );
                return;
            case 4:
                egret.Tween.get( trage ).to( { x: trage.x + trage.scaleX * 100 }, time );
                return;
        }
    }

    /**下一回合***/
    private nextRound(): void
    {
        this._round++;
        this._times = 0;
        this._totalTimes = 0;
        var arr: any[] = <any[]>this._roundData[this._round.toString()];
        if ( arr && arr.length == 2 )
        {////设置 战斗者数据   目前只会有两个战斗者
            if ( arr[0].playerid == Players.getInstance().rid )
            {
                this._mineRoundData = arr[0];
                this._enemyRoundData = arr[1];
                this._mineSkillData = Skills.getInstance().getSkillData( this._mineRoundData["skillid"] );
                this._enemySkillData = Skills.getInstance().getSkillData( this._enemyRoundData["skillid"] );
            }
            else
            {
                this._mineRoundData = arr[1];
                this._enemyRoundData = arr[0];
                this._mineSkillData = Skills.getInstance().getSkillData( this._mineRoundData["skillid"] );
                this._enemySkillData = Skills.getInstance().getSkillData( this._enemyRoundData["skillid"] );
            }
            /**设置播放时间**/
            this.setPlayTime();
        }
    }
    
    /**设置播放时间**/
    private setPlayTime(): void
    {
        /**设置该回合总播放时间**/
        if ( this._mineSkillData.actkeyTime > this._enemySkillData.actkeyTime )
        {
            this._totalTimes += this._mineSkillData.actkeyTime;
            this._inActTime = this._mineSkillData.actkeyTime;
            this._myPlayActTime = 0;
            this._enemyPlayActTime = this._mineSkillData.actkeyTime - this._enemySkillData.actkeyTime;
        }
        else
        {
            this._totalTimes += this._enemySkillData.actkeyTime;
            this._inActTime = this._enemySkillData.actkeyTime;
            this._enemyPlayActTime = 0;
            this._myPlayActTime = this._enemySkillData.actkeyTime - this._mineSkillData.actkeyTime;
        }

        if ( this._mineSkillData.inactTime > this._enemySkillData.inactTime )
            this._totalTimes += this._mineSkillData.inactTime;
        else
            this._totalTimes += this._enemySkillData.inactTime;
    }
    
    /**移动战斗者**/
    private moveFighter(): void
    {
        /**战斗者移动**/
        if ( ( this._mineSkillData.actMove + this._myPlayActTime ) == ( this._enemySkillData.actMove + this._enemyPlayActTime ) && this._times == ( this._mineSkillData.actMove + this._myPlayActTime ) )
        {//两个同时移动，则要取两者的中间点
            if ( this._mine.y < this._enemy.y )
                this._valY = this._mine.y + ( ( this._enemy.y - this._mine.y ) >> 1 );
            else
                this._valY = this._enemy.y + ( ( this._mine.y - this._enemy.y ) >> 1 );
            if ( this._mine.x < this._enemy.x )
            {
                this._valX = this._mine.x + ( ( this._enemy.x - this._mine.x ) >> 1 );
                egret.Tween.get( this._mine ).to( { x: this._valX - 20, y: this._valY }, 100 );
                egret.Tween.get( this._enemy ).to( { x: this._valX + 20, y: this._valY }, 100 );
            }
            else
            {
                this._valX = this._enemy.x + ( ( this._mine.x - this._enemy.x ) >> 1 );
                egret.Tween.get( this._mine ).to( { x: this._valX + 20, y: this._valY }, 100 );
                egret.Tween.get( this._enemy ).to( { x: this._valX - 20, y: this._valY }, 100 );
            }
        }
        else if ( this._mineSkillData.actMove != 0 && ( this._mineSkillData.actMove + this._myPlayActTime ) == this._times )
        {//单独移动我
            if ( this._mine.x < this._enemy.x )
                egret.Tween.get( this._mine ).to( { x: this._enemy.x - 20, y: this._enemy.y }, 100 );
            else
                egret.Tween.get( this._mine ).to( { x: this._enemy.x + 20, y: this._enemy.y }, 100 );
        }
        else if ( this._enemySkillData.actMove != 0 && ( this._enemySkillData.actMove + this._enemyPlayActTime ) == this._times )
        {//单独移动敌人
            if ( this._mine.x > this._enemy.x )
                egret.Tween.get( this._enemy ).to( { x: this._mine.x - 20, y: this._enemy.y }, 100 );
            else
                egret.Tween.get( this._enemy ).to( { x: this._mine.x + 20, y: this._enemy.y }, 100 );
        }
    }

    public dispose(): void
    {
        this.overFight();
        while ( this.numChildren > 0 )
            this.removeChildAt( 0 );
    }
}
