/**
 * 资源加载器
 * @author 
 *
 */
class ResLoader
{
    private static _instance: ResLoader;
    private static _allowInstance: Boolean;
    private loadingView: LoadingUI;
    private _completeDic: Object;

    public constructor()
    {
        if(!ResLoader._allowInstance)
            return;
        this.loadingView = new LoadingUI();
        this._completeDic = new Object();
    }
	
    /**加载资源组**/
    public loadGroup(group: string): void
    {
        if(this._completeDic.hasOwnProperty(group)) {
            //已经加载了
            GameBus.getInstance().dispatchEvent(new GameEvent(GameEvent.ResGroupLoaded, group));
            return;
        }

        RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onComplete, this);
        RES.addEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onLoadError, this);
        RES.addEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onProgress, this);
        RES.loadGroup(group);
        GameLayer.getInstance().viewLayer.visible = false;
        GameLayer.getInstance().uiLayer.visible = false;
        if(GameLayer.getInstance().alertLayer.contains(this.loadingView) == false)
            GameLayer.getInstance().alertLayer.addChild(this.loadingView);
    }
    
    /***加载完成**/
    private onComplete(event: RES.ResourceEvent): void
    {
        RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onComplete, this);
        RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onLoadError, this);
        RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onProgress, this);
        GameLayer.getInstance().viewLayer.visible = true;
        GameLayer.getInstance().uiLayer.visible = true;
        if(GameLayer.getInstance().alertLayer.contains(this.loadingView))
            GameLayer.getInstance().alertLayer.removeChild(this.loadingView);
        
        this._completeDic[event.groupName] = event.groupName;
        GameBus.getInstance().dispatchEvent(new GameEvent(GameEvent.ResGroupLoaded, event.groupName));
    }
    
    /***加载进程**/
    private onProgress(event: RES.ResourceEvent): void
    {
        this.loadingView.setProgress(event.itemsLoaded, event.itemsTotal);
    }
    
    /*****加载出错*/
    private onLoadError(event: RES.ResourceEvent): void
    {
        GameLayer.getInstance().viewLayer.visible = true;
        GameLayer.getInstance().uiLayer.visible = true;
        if(GameLayer.getInstance().alertLayer.contains(this.loadingView))
            GameLayer.getInstance().alertLayer.removeChild(this.loadingView);
    }
    
    /****是否加载完资源组***/
    public hasGroup(group: string): Boolean
    {
        if(this._completeDic && this._completeDic.hasOwnProperty(group))
            return true;
        else
            return false;
    }

    public static getInstance(): ResLoader
    {
        if(this._instance == null) {
            this._allowInstance = true;
            this._instance = new ResLoader();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
