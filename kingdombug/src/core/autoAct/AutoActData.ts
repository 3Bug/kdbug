/**
 * 自动播放动作数据
 * @swyan 
 *
 */
class AutoActData
{
    
    /**是否移动**/
    public isMove: number = 0;
        
    /***几率**/
    public rate: number = 0;
        
    /**动作**/
    public act: string = "";
    
    /***动画播放次数**/
    public times: number = 0;

    public constructor( act: string = "", rate: number = 0, isMove: number = 0, times: number = 0 )
    {
        this.act = act;
        this.rate = rate;
        this.isMove = isMove;
        this.times = times;
    }
}
