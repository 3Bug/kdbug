/**
 * 自动播放动画
 * @swyan 
 *
 */
class AutoActs
{
    private static _instance: AutoActs;
    private static _allowInstance: Boolean;
    
    /**总几率**/
    private _maxRate: number = 0;

    public constructor()
    {
        if ( !AutoActs._allowInstance )
            return;
    }
    
    /**获取触发几率   (百分比)*/
    public getRate( rate: number ): number
    {
        return Math.round(( rate / this.maxRate ) * 100 );
    }
    
    /**获取触发动作*/
    public getAct(): AutoActData
    {
        var key: number = Math.round( Math.random() * this.maxRate );
        var arr: AutoActData[] = AutoActXX.actArr;
        for ( var i: number = 0; i < arr.length; i++ )
        {
            if ( key <= arr[i].rate )
                return arr[i];
            else
                key -= arr[i].rate;
        }
        return null;
    }
    
    /**总几率基数**/
    public get maxRate(): number
    {
        if ( this._maxRate == 0 || this._maxRate == NaN || this._maxRate == null )
        {
            var arr: AutoActData[] = AutoActXX.actArr;
            for ( var i: number = 0; i < arr.length; i++ )
            {
                this._maxRate += arr[i].rate;
            }
        }
        return this._maxRate;
    }

    public static getInstance(): AutoActs
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new AutoActs();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
