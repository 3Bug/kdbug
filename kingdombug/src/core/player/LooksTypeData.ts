/**
 * 外观类型数据
 * @swyan 
 *
 */
class LooksTypeData {
    public looksNum: number;
    public looksStr: string;
    
	public constructor(looksNum:number,looksStr:string) {
        this.looksNum = looksNum;
        this.looksStr = looksStr;
	}
}
