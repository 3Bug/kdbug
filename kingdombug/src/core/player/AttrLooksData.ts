/**
 * 属性外观数据
 * @swyan 
 */
class AttrLooksData
{

    public attr: string;

    public attrKey: number;

    public looksType: number;

    public looks: number;

    public constructor( attr: string, attrKey: number, looksType: number, looks: number )
    {
        this.attr = attr;
        this.attrKey = attrKey;
        this.looksType = looksType;
        this.looks = looks;
    }
}
