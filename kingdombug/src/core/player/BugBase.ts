/**
 * Bug基本数据
 * @swyan 
 *
 */
class BugBase
{
    
    /***等级**/
    public lv: number = 0;
    
    /**姓名**/
    public name: string = "";
    
    /**血量**/
    public playerhealth: number = 0;
    
    /**经验**/
    public exp: number = 0;
    
    /**金币**/
    public gold: number = 0;
    
    /**力量*/
    public playerpower: number = 0;
    
    /**智力**/
    public playerintellige: number = 0;
    
    /**脑残值**/
    public playernc: number = 0;
    
    /**gay值**/
    public playergay: number = 0;
        
    /***头**/
    public head: number = 0;
        
    /***身体**/
    public body: number = 0;
        
    /***左上臂**/
    public armUpperL: number = 0;
        
    /***右上臂**/
    public armUpperR: number = 0;
        
    /***左下臂**/
    public armLowerL: number = 0;
        
    /***右下臂**/
    public armLowerR: number = 0;
        
    /***左手掌**/
    public handL: number = 0;
            
    /***右手掌**/
    public handR: number = 0;
        
    /***左上腿**/
    public legUpperL: number = 0;
            
    /***右上腿**/
    public legUpperR: number = 0;
            
    /***左下腿**/
    public legLowerL: number = 0;
            
    /***右下腿**/
    public legLowerR: number = 0;
            
    /***左脚掌**/
    public footL: number = 0;
                
    /***右脚掌**/
    public footR: number = 0;
    
    /***面**/
    public face: number = 0;
    
    /**潜能点**/
    public point: number = 0;

    public constructor()
    {
    }
    
    /**根据协议数据类型设置数据*/
    public setProtocolData( data: Object ): void
    {
        /*角色数据协议
        playername,playerlv,playerexperienc,playerbyte,playerhealth,
        playerpower,playerintellige,playernc,playergay,playerhead,playerface,
        playerbody,playerarmupperl,playerarmupperr,playerarmlowerl,playerarmlowerr,
        playerhandl,playerhandr,playerlegupperl,playerlegupperr,playerleglowerl,
        playerleglowerr,playerfootl,playerfootr,playerpoint
        */
        if ( data == null )
            return;

        this.name = data["playername"];
        if ( this.lv != data["playerlv"] )
        {
            this.lv = data["playerlv"];
            GameBus.getInstance().dispatchEvent( new PlayerEvent( PlayerEvent.BugLvUp ) );
        }

        this.exp = data["playerexperienc"];
        this.gold = data["playerbyte"];
        this.playerhealth = data["playerhealth"];
        this.playerpower = data["playerpower"];
        this.playerintellige = data["playerintellige"];
        this.playernc = data["playernc"];
        this.playergay = data["playergay"];
        /*this.head = data["playerhead"];
        this.face = data["playerface"];
        this.body = data["playerbody"];
        this.armUpperL = data["playerarmupperl"];
        this.armUpperR = data["playerarmupperr"];
        this.armLowerL = data["playerarmlowerl"];
        this.armLowerR = data["playerarmlowerr"];
        this.handL = data["playerhandl"];
        this.handR = data["playerhandr"];
        this.legUpperL = data["playerlegupperl"];
        this.legUpperR = data["playerlegupperr"];
        this.legLowerL = data["playerleglowerl"];
        this.legLowerR = data["playerleglowerr"];
        this.footL = data["playerfootl"];
        this.footR = data["playerfootr"];
        this.point = data["playerpoint"];*/
    }
}
