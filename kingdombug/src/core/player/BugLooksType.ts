/**
 * BUG外观类型
 * @swyan 
 *
 */
class BugLooksType
{
    
    /***头**/
    public static head: number = 1;
    
    /***身体**/
    public static body: number = 2;
    
    /***左上臂**/
    public static armUpperL: number = 3;
    
    /***右上臂**/
    public static armUpperR: number = 4;
    
    /***左下臂**/
    public static armLowerL: number = 5;
    
    /***右下臂**/
    public static armLowerR: number = 6;
    
    /***左手掌**/
    public static handL: number = 7;
        
    /***右手掌**/
    public static handR: number = 8;
    
    /***左上腿**/
    public static legUpperL: number = 9;
        
    /***右上腿**/
    public static legUpperR: number = 10;
        
    /***左下腿**/
    public static legLowerL: number = 14;
        
    /***右下腿**/
    public static legLowerR: number = 12;
        
    /***左脚掌**/
    public static footL: number = 13;
            
    /***右脚掌**/
    public static footR: number = 14;
    
    /***面**/
    public static face: number = 15;
    
    /**外观类型数组   包含所有外观**/
    private static _looksArr: number[] = null;
    
    /***获取资源名字关键字  用于读取资源**/
    public static getNameKey(type: number): string
    {
        switch(type) {
            case this.head:
                return "head";
            case this.body:
                return "body";
            case this.armUpperL:
                return "armUpperL";
            case this.armUpperR:
                return "armUpperR";
            case this.armLowerL:
                return "armLowerL";
            case this.armLowerR:
                return "armLowerR";
            case this.handL:
                return "handL";
            case this.handR:
                return "handR";
            case this.legUpperL:
                return "legUpperL";
            case this.legUpperR:
                return "legUpperR";
            case this.legLowerL:
                return "legLowerL";
            case this.legLowerR:
                return "legLowerR";
            case this.footL:
                return "footL";
            case this.footR:
                return "footR";
            case this.face:
                return "face";
        }
        return "";
    }

    /**外观类型数组   包含所有外观类型**/
    public static get looksArr(): number[]
    {
        if(this._looksArr == null) {
            this._looksArr = [this.head, this.armUpperL, this.armUpperR, this.armLowerL, this.armLowerR,
                this.handL, this.handR, this.legUpperL, this.legUpperR, this.legLowerL, this.legLowerR, this.footL, this.footR, this.face];
        }
        return this._looksArr;
    }

    public constructor()
    {
    }
}
