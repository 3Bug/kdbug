/**
 * 玩家数据类
 * @swyan 
 *
 */
class Players
{
    private static _instance: Players;
    private static _allowInstance: Boolean;
    
    /*创建角色容器**/
    private _createPanel: CreatePlayerPanel;
    
    /**角色Id   token**/
    public rid: string = "";
    
    /**服务器Id**/
    private _srvId: number;
    
    /***Bug数据**/
    private _bug: BugBase;
    
    /**是否已经登录*/
    public isLogin: boolean;
    
    public constructor()
    {
        if ( !Players._allowInstance )
            return;
            GameBus.getInstance().addEventListener( SocketEvent.ReceiveProtocol, this.receive, this );
            GameBus.getInstance().addEventListener( GameEvent.GameStart, this.gameStart, this );
        }

    /***设置玩家数据**/
    public setPlayer( rid: string, srvId: number, bug: BugBase ): void
    {
        this.rid = rid;
        this._srvId = srvId;
        this._bug = bug;
    }

    /**服务器Id**/
    public get srvId(): number
    {
        return this._srvId;
    }

    /***Bug数据**/
    public get bug(): BugBase
    {
        if ( this._bug == null )
            this._bug = new BugBase();
        return this._bug;
    }
    
    /**游戏开始*/
    private gameStart(): void
    {
        GameBus.getInstance().removeEventListener( GameEvent.GameStart, this.gameStart, this );
        var obj: Object = new Object();
        obj["playerid"] = this.rid;
        Socket.getInstance().sendProtocol( Protocol.LOGIN_STATUS, obj );
        console.log( "请求登录" );

        HttpPost.getInstance();
    }
    
    /**接收协议**/
    private receive( event: egret.Event ): void
    {
        var data: Object;

        if ( event.data == Protocol.PLAYER_INFO )
        {
            //角色数据协议
            data = ProtocolInfo.getInstance().getProtocol( Protocol.PLAYER_INFO );
            if ( data )
            {
                this.rid = data["playerid"];
                this.bug.setProtocolData( data );
                GameBus.getInstance().dispatchEvent( new PlayerEvent( PlayerEvent.Update ) );
                //请求背包数据
                Socket.getInstance().sendProtocol(Protocol.BACKPACK,null);
            }
        }

        if ( event.data == Protocol.CREATE_PLAYER )
        {
            //创建角色
            data = ProtocolInfo.getInstance().getProtocol( Protocol.CREATE_PLAYER );
            if ( data )
            {
                data = ProtocolInfo.getInstance().getProtocol( Protocol.CREATE_PLAYER );
                if ( data == null )
                    return;
                if ( this._createPanel && data["result"] == 1 )
                {
                    this.closeCreatePanel();
                    this.reqPlayerData();
                }
                else
                    OperateTipMgr.getInstance().showTips( data["msg"] );
            }
        }

        if ( event.data == Protocol.LOGIN_STATUS )
        {
            //创建角色结果
            //console.log( "返回创建角色结果" );
            data = ProtocolInfo.getInstance().getProtocol( Protocol.LOGIN_STATUS );
            if ( data )
            {
                if ( data["result"] == 2 )
                {
                    if ( this._createPanel == null )
                        this._createPanel = new CreatePlayerPanel( data["playerpoint"] );
                    var sp: egret.Sprite;
                    sp = GameLayer.getInstance().viewLayer;
                    this._createPanel.show();
                    sp.addChild( this._createPanel );//添加到最上层
                    PlotMgr.getInstance().checkPlot( 1 );
                }
                else if ( data["result"] == 1 )
                {
                    this.reqPlayerData();
                }
                else if ( data["result"] == 0 )
                {
                    console.log( "登录失败：" + data["msg"] );
                }
            }
        }
    }

    private reqPlayerData(): void
    {
        var obj: Object = new Object();
        obj["playerid"] = this.rid;
        Socket.getInstance().sendProtocol( Protocol.PLAYER_INFO, obj );
    }

    /**关闭创建角色容器**/
    private closeCreatePanel(): void
    {
        if ( this._createPanel )
        {
            this._createPanel.hide();
            egret.Tween.get( this._createPanel )
                .to( { scaleX: 0.3, scaleY: 0.3 }, 200, egret.Ease.backIn )
                .call( this.removeCreatePanel, this );
        }
    }
    
    /**移除创建角色容器**/
    private removeCreatePanel(): void
    {
        egret.Tween.removeTweens( this._createPanel );
        if ( GameLayer.getInstance().viewLayer.contains( this._createPanel ) )
            GameLayer.getInstance().viewLayer.removeChild( this._createPanel );
    }
    
    /***获取预览外观值    {looksType:"***",looks:0}**/
    public getProLooksNum( attrType: string, key: number ): Object
    {
        var looks: Object = new Object();
        var tempKey: number = -1;
        var arr: AttrLooksData[] = AttrLooksXX.attrLooksArr;
        if ( arr )
        {
            for ( var i: number = 0; i < arr.length; i++ )
            {
                if ( arr[i].attr == attrType && key >= arr[i].attrKey && tempKey < arr[i].attrKey )
                {
                    tempKey = arr[i].attrKey;
                    looks["looksType"] = arr[i].looksType;
                    looks["looks"] = arr[i].looks;
                }
            }
        }
        return looks;
    }

    public static getInstance(): Players
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Players();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
