/**
 * 单个技能数据
 * @swyan 
 *
 */
class SkillData
{
    /**技能id**/
    public id: number = 0;
    
    /**进名称*/
    public name: string = "";
    
    /**技能类型**/
    public type: number = 0;
    
    /**技能动作**/
    public act: number = 0;
    
    /**技能动作持续时间（百毫秒）**/
    public actTime: number = 0;
    
    /**攻击时移动到对方面前（百毫秒  0不移动）**/
    public actMove: number = 0;
    
    /**攻击动作伤害时间点(百毫秒,从攻击动作开始算)**/
    public actkeyTime: number = 0;
    
    /**受击动作**/
    public inact: number = 0;
    
    /**受击动作时间(百毫秒)**/
    public inactTime: number = 0;
    
    /**受击移动(0不移动 1向后,2后上，3后下，4前,5后跨屏，6后上跨屏，7后下跨屏)**/
    public inactMove: number = 0;
    
    /**攻击特效**/
    public eff: number = 0;
    
    /**特效对象(0自己,1对方)**/
    public effTar: number = 0;

    public constructor( id: number = 0, name: string = "", type: number = 0, act: number = 0,
        actTime: number = 0, actMove: number = 0, actKeyTime: number = 0, inact: number = 0,
        inactTime:number = 0,inactMove: number = 0, eff: number = 0, effTar: number = 0 )
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.act = act;
        this.actTime = actTime;
        this.actMove = actMove;
        this.actkeyTime = actKeyTime;
        this.inact = inact;
        this.inactTime = inactTime; 
        this.inactMove = inactMove;
        this.eff = eff;
        this.effTar = effTar;
    }
}
