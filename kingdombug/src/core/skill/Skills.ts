/**
 * 技能数据
 * @swyan 
 *
 */
class Skills
{
    private static _instance: Skills;
    private static _allowInstance: Boolean;
    
    /***技能数据**/
    private _skillList: Object;

    public constructor()
    {
        if ( !Skills._allowInstance )
            return;
    }

    public static getInstance(): Skills
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Skills();
            this._allowInstance = false;
        }
        return this._instance;
    }

    /***获取技能数据**/
    public getSkillData( id: number ): SkillData
    {
        if ( this.skillList.propertyIsEnumerable( id.toString() ) )
            return this.skillList[id.toString()];
        else
            return null;
    }

    public get skillList(): Object
    {
        if ( this._skillList == null )
        {
            this._skillList = new Object();
            var arr: SkillData[] = SkillXX.skillList;
            for ( var i: number = 0; i < arr.length; i++ )
            {
                this._skillList[arr[i].id.toString()] = arr[i];
            }
        }
        return this._skillList;
    }
}
