/**
 * 单个剧情数据
 * @swyan 
 *
 */
class PlotData
{
    /**剧情类型   1黑幕剧情   2对话剧情**/
    public plotType: number = 0;
    
    /**序列**/
    public index: number = 0;
    
    /**触发类型   1等级  2血量**/
    public trigger: number = 0;
    
    /***触发关键值**/
    public key: number = 0;
    
    /**角色类型   用于显示角色对话图片**/
    public roleType: number = 0;
    
    /***位置   对话模式下   1左  2右**/
    public pos: number = 0;
    
    /**内容描述**/
    public desc: string = "";


    public constructor( plotType?: number, index?: number, trigger?: number, key?: number, roleType?: number,pos?: number, desc?: string )
    {
        this.plotType = plotType;
        this.index = index;
        this.trigger = trigger;
        this.key = key;
        this.roleType = roleType;
        this.pos = pos;
        this.desc = desc;
    }
}
