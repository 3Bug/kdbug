/**
 * 剧情数据类
 * @swyan 
 *
 */
class Plots
{
    private static _instance: Plots;
    private static _allowInstance: Boolean;
    
    /**已处理的剧情数据**/
    private _plotDic: PlotData[][] = [];

    public constructor()
    {
        if ( !Plots._allowInstance )
            return;
        this.readPlotData();
    }
    /***读取剧情数据**/
    private readPlotData(): void
    {
        this._plotDic = [];
        var arr: PlotData[] = PlotXX.plotArr;
        var key: string = "";
        for ( var i: number = 0; i < arr.length; i++ )
        {
            key = arr[i].trigger + "_" + arr[i].key;
            if ( this._plotDic[key] )
                this._plotDic[key].push( arr[i] );
            else
                this._plotDic[key] = [arr[i]];
        }
    }
    
    /***读取剧情数据**/
    /*public readPlotData(): void
    {
        if ( this.plotArr != null )
            return;//只有没有数据时才会去拿数据
        var buff: ArrayBuffer = RES.getRes( "plot_xx" );
        if ( buff == null )
            return;
        var data: egret.ByteArray = new egret.ByteArray( buff );
        if ( data == null || data.length <= 0 )
            return;
        var pData: PlotData;
        this.plotArr = [];
        var len: number = data.readInt();
        console.log( len );//这里读取到的数据不正确
        for ( var i: number = 0; i < len; i++ )
        {
            pData = new PlotData();
            pData.plotType = data.readInt();
            pData.trigger = data.readInt();
            pData.key = data.readInt();
            pData.roleType = data.readInt();
            pData.desc = data.readUTF();
            this.plotArr[pData.trigger + "_" + pData.key] = pData;
            console.log( pData.key + pData.desc );
        }
    }*/
    
    /**获取剧情数据   trugger 触发类型 具体看plot_data表 key关键值    **/
    public getPlot( trigger: number, key: number ): PlotData[]
    {
        var p: PlotData[] = null;
        var str: string = trigger + "_" + key;
        if ( this._plotDic[str] )
            p = this._plotDic[str];
        if ( p && p.length > 1 )
        {//两个数据以上 要排序
            p.sort( function ( a, b )
            {
                return a.index - b.index
            });
        }
        return p;
    }

    public static getInstance(): Plots
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Plots();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
