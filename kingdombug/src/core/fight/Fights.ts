/**
 * 战斗数据
 * @swyan 
 *
 */
class Fights
{
    private static _instance: Fights;
    private static _allowInstance: Boolean;
    private _fightView: FightView;
    public isFighting: boolean;
    public pvp: number = 1;
    public pve: number = 2;
    public constructor()
    {
        if ( !Fights._allowInstance )
            return;
        this.isFighting = false;
        GameBus.getInstance().addEventListener( SocketEvent.ReceiveProtocol, this.receive, this );
        GameBus.getInstance().addEventListener( FightEvent.Over, this.hideFight, this );
    }
    public static getInstance(): Fights
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Fights();
            this._allowInstance = false;
        }
        return this._instance;
    }
    
    /**接收协议**/
    private receive( event: egret.Event ): void
    {
        var data: any;

        if ( event.data == Protocol.START_BATTLE )
        {
            data = ProtocolInfo.getInstance().getProtocol( Protocol.START_BATTLE );
            if ( data && data["result"] == 0 )
                OperateTipMgr.getInstance().showTips( data["msg"] );
        }

        if ( event.data == Protocol.START_BATTLE )
        {
            data = ProtocolInfo.getInstance().getProtocol( Protocol.START_BATTLE );
            if ( data == null )
                return;
            this.showFight( <any[]>data["battleData"], this.pvp );
        }
    }

    /**显示战斗**/
    private showFight( battleData: any[], type: number ): void
    {
        if ( battleData == null )
            return;
        this.isFighting = true;
        GameLayer.getInstance().sceneLayer.visible = false;
        GameLayer.getInstance().viewLayer.visible = false;
        GameLayer.getInstance().uiLayer.visible = false;
        GameLayer.getInstance().plotLayer.visible = false;
        if ( this._fightView == null )
            this._fightView = new FightView();
        this._fightView.open( battleData, type );
        if ( GameLayer.getInstance().fightLayer.contains( this._fightView ) == false )
            GameLayer.getInstance().fightLayer.addChild( this._fightView );
    }

    /**隐藏战斗**/
    private hideFight(): void
    {
        this.isFighting = false;
        GameLayer.getInstance().sceneLayer.visible = true;
        GameLayer.getInstance().viewLayer.visible = true;
        GameLayer.getInstance().uiLayer.visible = true;
        GameLayer.getInstance().plotLayer.visible = true;
        this._fightView.hide();
        if ( GameLayer.getInstance().fightLayer.contains( this._fightView ) )
            GameLayer.getInstance().fightLayer.removeChild( this._fightView );
    }
}
