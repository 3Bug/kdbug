/**
 * 操作提示管理器
 * @swyan 
 *
 */
class OperateTipMgr
{
    private static _instance: OperateTipMgr;
    private static _allowInstance: Boolean;

    private _tips: OperateTipsPanel;
    private _times: number;

    public constructor()
    {
        if ( !OperateTipMgr._allowInstance )
            return;
    }

    public static getInstance(): OperateTipMgr
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new OperateTipMgr();
            this._allowInstance = false;
        }
        return this._instance;
    }
    
    /**操作提示**/
    public showTips( str: string ): void
    {
        if ( str == "" )
            return;
        if ( this._tips == null )
            this._tips = new OperateTipsPanel();
        this._tips.setContent( str );
        this._tips.alpha = 1;
        TimerMgr.getInstance().remove( this.timer );
        egret.Tween.removeTweens( this._tips );
        this._times = 0;
        TimerMgr.getInstance().add( TimerMgr.t1000, this.timer, this );
        this._tips.x = ( Config.stageWid - this._tips.width ) >> 1;
        this._tips.y = Config.stageHei >> 1;
        if ( GameLayer.getInstance().alertLayer.contains( this._tips ) == false )
            GameLayer.getInstance().alertLayer.addChild( this._tips );
    }
    
    /**计时器**/
    private timer(): void
    {
        if ( this._times >= 2 )
        {
            TimerMgr.getInstance().remove( this.timer );
            egret.Tween.get( this._tips ).to( { alpha: 0.2 }, 1000 ).call( this.tweenCom, this );
            return;
        }
        this._times++;
    }

    private tweenCom(): void
    {
        egret.Tween.removeTweens( this._tips );
        if ( GameLayer.getInstance().alertLayer.contains( this._tips ) )
            GameLayer.getInstance().alertLayer.removeChild( this._tips );
    }
}
