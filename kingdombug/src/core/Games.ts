/**
 * 游戏总体数据
 * @swyan 
 *
 */
class Games {
    
    /**龙骨动画缓存管理器*/
    public animationCacheMgr: dragonBones.AnimationCacheManager;
    
    private static _instance: Games;
    private static _allowInstance: Boolean;
    public constructor() {
        if(!Games._allowInstance)
            return;
    }

    public static getInstance(): Games {
        if(this._instance == null) {
            this._allowInstance = true;
            this._instance = new Games();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
