/**
 * 单个物品数据
 * @swyan 
 *
 */
class ItemData {
    
    /****物品id*/
    public id: number = 0;
    
    /***类型**/
    public type: number = 0;
    
    /***等级**/
    public lev: number = 0;
    
    /**数量**/
    public num: number = 0;
    
	public constructor() {
	}
}
