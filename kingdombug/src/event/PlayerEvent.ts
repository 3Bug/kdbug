/**
 * 角色事件
 * @swyan 
 *
 */
class PlayerEvent extends egret.Event{
    
    /***BUG数据更新***/
    public static Update: string = "PlayerEvent.Update";
    
    
    /***BUG等级提升***/
    public static BugLvUp: string = "PlayerEvent.BugLvUp";
    
    public data: any;
	public constructor(type:string,data?:any) {
        super(type);
        this.data = data;
	}
}
