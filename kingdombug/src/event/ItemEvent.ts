/**
 * 物品事件
 * @swyan 
 *
 */
class ItemEvent extends egret.Event{
    
    /***初始化物品列表***/
    public static Init: string = "ItemEvent.Init";
    
    public data: any;
    public constructor(type:string,data?:any) {
        super(type);
        this.data = data;
    }
}
