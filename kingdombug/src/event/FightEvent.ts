/**
 * 战斗事件
 * @swyan 
 *
 */
class FightEvent extends egret.Event{
    
    /***战斗开始***/
    public static Start: string = "FightEvent.Start";
        
    /***战斗结束***/
    public static Over: string = "FightEvent.Over";
    
    public data: any;
    public constructor(type:string,data?:any) {
        super(type);
        this.data = data;
    }
}
