/**
 * 登录事件
 * @swyan 
 *
 */
class LoginEvent extends egret.Event
{
    /***检测是否已登录***/
    public static CheckLogin: string = "GameEvent.CheckLogin";
    
    /***登录***/
    public static Login: string = "GameEvent.Login";
    
    public data: any;
    public constructor( type: string, data?: any )
    {
        super( type );
        this.data = data;
    }
}
