/**
 * socket事件
 * @Swyin
 * 2015-8-25
 */
class SocketEvent extends egret.Event
{
    public static CONNECT: string = "SocketEvent.CONNECT";
    public static CLOSE: string = "SocketEvent.CLOSE";
    public static IO_ERROR: string = "SocketEvent.IO_ERROR";
    
    /**接收协议**/
    public static ReceiveProtocol: string = "SocketEvent.ReceiveProtocol";
    
    private _data: any;
    public constructor( type: string, data: any = null )
    {
        super( type );
        this._data = data;
    }

    public get data(): any
    {
        return this._data;
    }
}
