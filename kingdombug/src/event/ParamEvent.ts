/**
 * 带参数事件
 * @swyan 
 *
 */
class ParamEvent extends egret.Event {
    private _data: any;

    public constructor(type: string,data: any) {
        super(type);
        this._data = data;
    }

    public get data(): any {
        return this._data;
    }
}
