/**
 * 剧情事件
 * @swyan 
 *
 */
class PlotEvent extends egret.Event{
        
    /***剧情播放结束***/
    public static PlayPlotOver: string = "PlotEvent.PlayPlotOver";
        
    public data: any;
    public constructor(type:string,data?:any) {
        super(type);
        this.data = data;
    }
}
