/**
 * 游戏总事件
 * @swyan 
 *
 */
class GameEvent extends egret.Event{
    /***游戏开始***/
    public static GameStart: string = "GameEvent.GameStart";
    
    /***游戏结束***/
    public static GameOver: string = "GameEvent.GameOver";
    
    /***加载资源组完成**/
    public static ResGroupLoaded: string = "GameEvent.ResGroupLoaded";
    
    public data: any;
	public constructor(type:string,data?:any) {
        super(type);
        this.data = data;
	}
}
