/**
 * 界面事件
 * @swyan 
 *
 */
class ViewEvent extends egret.Event {
    /***打开界面***/
    public static OpenView: string = "ViewEvent.OpenView";
        
    /***关闭界面***/
    public static CloseView: string = "ViewEvent.CloseView";

    public data: any;
    public viewType: number;
    public constructor(type: string,viewType: number,data?: any) {
        super(type);
        this.data = data;
        this.viewType = viewType;
    }
}
