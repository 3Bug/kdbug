/**
 * 登录器
 * @swyan 
 *
 */
class Login
{
    private static _instance: Login;
    private static _allowInstance: Boolean;
    public constructor()
    {
        if ( !Login._allowInstance )
            return;
    }
    
    //检测是否已登录
    public checkLogin(): void
    {
        nest.user.checkLogin( null, function ( data )
        {
            GameBus.getInstance().dispatchEvent( new LoginEvent( LoginEvent.CheckLogin, data ) );
            //console.log( "!!!!!nest.user.login" + JSON.stringify( data ) );
        });
    }

    /**登录游戏**/
    public login(): void
    {
        var loginInfo: nest.user.LoginInfo = {};
        nest.user.login( loginInfo, function ( data )
        {
            GameBus.getInstance().dispatchEvent( new LoginEvent( LoginEvent.Login, data ) );
            //console.log( "!!!!!nest.user.login" + JSON.stringify( data ) );
        })
    }
    
    //检测功能支持
    public support(): void
    {
        nest.share.isSupport( function ( data )
        {
            console.log( "cm old share " + JSON.stringify( data ) )
        })
        nest.app.isSupport( function ( data )
        {
            console.log( "cm old app  " + JSON.stringify( data ) )
        })
    }
    
    //判断支持那种登录方式
    public loginSupport(): void
    {
        nest.user.isSupport( function ( data )
        {
            console.log( data );
        })
    }
        
    //充值调用
    public pay(): void
    {
        var payInfo: nest.iap.PayInfo = {
            goodsId: "8",
            goodsNumber: "1",
            serverId: "1",
            ext: "fdsfdsf"
        };

        nest.iap.pay( payInfo, function ( data )
        {
            console.log( data );
        })
    }
            
    //分享信息
    public share(): void
    {

        var data: nest.share.ShareInfo = {
            title: "分享游戏得元宝",
            description: "了",
            img_title: "BUG帝国",
            img_url: "http://sglqxz.egret-labs.org/common/glqxzshareicon.png",
            url: "http://glqxzqqb.gz.1251278653.clb.myqcloud.com/share.html"
        };
        nest.share.share( data, function ( data )
        {
            console.log( data );
        })
    }
                    
    //获取好友
    public friends(): void
    {
        var data = {};
        nest.social.getFriends( data, function ( data )
        {
            console.log( data );
        })
    }
                        
    //生成桌面快捷图标
    public sendToDesktop(): void
    {
        var data = {};
        nest.app.sendToDesktop( data, function ( data )
        {
            console.log( data );
        })
    }
                            
    //打开bbs
    public openBBS(): void
    {
        nest.social.openBBS( null, function ( data )
        {
            console.log( data );
        })
    }


    public static getInstance(): Login
    {
        if ( this._instance == null )
        {
            this._allowInstance = true;
            this._instance = new Login();
            this._allowInstance = false;
        }
        return this._instance;
    }
}
