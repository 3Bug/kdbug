
var game_file_list = [
    //以下为自动修改，请勿修改
    //----auto game_file_list start----
	"libs/modules/egret/egret.js",
	"libs/modules/egret/egret.native.js",
	"libs/modules/game/game.js",
	"libs/modules/game/game.native.js",
	"libs/modules/tween/tween.js",
	"libs/modules/res/res.js",
	"libs/modules/dragonbones/dragonbones.js",
	"libs/modules/socket/socket.js",
	"libs/modules/nest/nest.js",
	"bin-debug/commons/Config.js",
	"bin-debug/commons/Conventor.js",
	"bin-debug/commons/CustomButton.js",
	"bin-debug/commons/DbMcOffset.js",
	"bin-debug/commons/DragonBonesMc.js",
	"bin-debug/commons/GameBus.js",
	"bin-debug/commons/GameLayer.js",
	"bin-debug/commons/HttpPost.js",
	"bin-debug/commons/ImgSprite.js",
	"bin-debug/commons/NumStepper.js",
	"bin-debug/commons/Protocol.js",
	"bin-debug/commons/ProtocolInfo.js",
	"bin-debug/commons/Reflection.js",
	"bin-debug/commons/ResGroup.js",
	"bin-debug/commons/ResourcePath.js",
	"bin-debug/commons/Socket.js",
	"bin-debug/commons/SoundMgr.js",
	"bin-debug/commons/SoundPlayer.js",
	"bin-debug/commons/StringUtil.js",
	"bin-debug/commons/TabBar.js",
	"bin-debug/commons/TabBarBtn.js",
	"bin-debug/commons/TimerMgr.js",
	"bin-debug/core/autoAct/AutoActData.js",
	"bin-debug/core/autoAct/AutoActs.js",
	"bin-debug/core/bag/Bags.js",
	"bin-debug/core/bag/ItemData.js",
	"bin-debug/core/fight/Fights.js",
	"bin-debug/core/Games.js",
	"bin-debug/core/player/AttrLooksData.js",
	"bin-debug/core/player/BugBase.js",
	"bin-debug/core/player/BugLooksType.js",
	"bin-debug/core/player/LooksTypeData.js",
	"bin-debug/core/player/Players.js",
	"bin-debug/core/plot/PlotData.js",
	"bin-debug/core/plot/Plots.js",
	"bin-debug/core/skill/SkillData.js",
	"bin-debug/core/skill/Skills.js",
	"bin-debug/core/tips/OperateTipMgr.js",
	"bin-debug/core/xx/AttrLooksXX.js",
	"bin-debug/core/xx/AutoActXX.js",
	"bin-debug/core/xx/PlotXX.js",
	"bin-debug/core/xx/SkillXX.js",
	"bin-debug/event/FightEvent.js",
	"bin-debug/event/GameEvent.js",
	"bin-debug/event/ItemEvent.js",
	"bin-debug/event/LoginEvent.js",
	"bin-debug/event/ParamEvent.js",
	"bin-debug/event/PlayerEvent.js",
	"bin-debug/event/PlotEvent.js",
	"bin-debug/event/SocketEvent.js",
	"bin-debug/event/ViewEvent.js",
	"bin-debug/view/base/PanelBase.js",
	"bin-debug/view/base/ViewType.js",
	"bin-debug/view/ui/UIBtnPanel.js",
	"bin-debug/GameStage.js",
	"bin-debug/LoadingUI.js",
	"bin-debug/Login.js",
	"bin-debug/Main.js",
	"bin-debug/MD5.js",
	"bin-debug/ResLoader.js",
	"bin-debug/view/bag/BagItemPanel.js",
	"bin-debug/view/base/ViewBase.js",
	"bin-debug/view/bag/BagView.js",
	"bin-debug/view/bag/ItemIcon.js",
	"bin-debug/view/bug/Bug.js",
	"bin-debug/view/create/CreatePlayerPanel.js",
	"bin-debug/view/FbView.js",
	"bin-debug/view/fight/Fighter.js",
	"bin-debug/view/fight/FightPanel.js",
	"bin-debug/view/fight/FightView.js",
	"bin-debug/view/login/LoginView.js",
	"bin-debug/view/MainView.js",
	"bin-debug/view/plot/DialoguePanel.js",
	"bin-debug/view/plot/PlotBlack.js",
	"bin-debug/view/plot/PlotDialogue.js",
	"bin-debug/view/plot/PlotMgr.js",
	"bin-debug/view/RoleView.js",
	"bin-debug/view/scene/BugArea.js",
	"bin-debug/view/scene/BugTalkPanel.js",
	"bin-debug/view/scene/SceneBgPanel.js",
	"bin-debug/view/scene/SceneView.js",
	"bin-debug/view/SettingView.js",
	"bin-debug/view/tips/OperateTipsPanel.js",
	"bin-debug/view/ui/UIBtn.js",
	"bin-debug/view/ui/UIPanel.js",
	//----auto game_file_list end----
];

var window = {};

egret_native.setSearchPaths([""]);

egret_native.requireFiles = function () {
    for (var key in game_file_list) {
        var src = game_file_list[key];
        require(src);
    }
};

egret_native.egretInit = function () {
    egret_native.requireFiles();
    egret.TextField.default_fontFamily = "/system/fonts/DroidSansFallback.ttf";
    //egret.dom为空实现
    egret.dom = {};
    egret.dom.drawAsCanvas = function () {
    };
};

egret_native.egretStart = function () {
    var option = {
        //以下为自动修改，请勿修改
        //----auto option start----
		entryClassName: "Main",
		frameRate: 30,
		scaleMode: "showAll",
		contentWidth: 480,
		contentHeight: 800,
		showPaintRect: false,
		showFPS: false,
		fpsStyles: "x:0,y:0,size:30,textColor:0x00c200,bgAlpha:0.9",
		showLog: false,
		logFilter: "",
		maxTouches: 2,
		textureScaleFactor: 1
		//----auto option end----
    };

    egret.native.NativePlayer.option = option;
    egret.runEgret();
    egret_native.Label.createLabel(egret.TextField.default_fontFamily, 20, "", 0);
    egret_native.EGTView.preSetOffScreenBufferEnable(true);
};